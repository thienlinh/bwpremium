
export const quantityValues = () => {
    const data = [];

    for (let index = 1; index <= 20; index++) {
        data.push({ key: index, label: +index });
    }

    return data;
};

export const pushValues = () => {
    const data = [];

    for (let index = -3; index <= 3; index++) {
        data.push({ key: index, label: +index });
    }

    return data;
};

export const film35mmKey = 'film35mm';
export const film120mmKey = 'film120mm';
export const filmLF45Key = 'filmLF45';
export const filmLF810Key = 'filmLF810';

export const film35mmDisplayName = '35mm black & white';
export const film120mmDisplayName = '120mm black & white';
export const filmLF45DisplayName = 'Large format 4x5';
export const filmLF810DisplayName = 'Large format 8x10';

export const pending = 'pending';
export const cancel = 'cancel';

export const ORDER_STATUS_CANCELED = 'canceled';
export const ORDER_STATUS_DONE = 'done';
export const ORDER_STATUS_PENDING = 'pending';

export const ORDER_TYPE_PACKAGE = 'package';
export const ORDER_TYPE_DIRECT = 'direct';

export const FILM_SIZE_135MM = 'Film 135mm';
export const FILM_SIZE_120MM = 'Film 120mm';
export const FILM_SIZE_LF45 = 'Film Large Format 4x5';
export const FILM_SIZE_LF810 = 'Film Large Format 8x10';

export const FILM_TYPE_COLOR = 'Color';
export const FILM_TYPE_BW = 'Black & White';
export const FILM_TYPE_VISION = 'Vision';
export const FILM_TYPE_SLIDE = 'Slide';

export const filmTypeList = [FILM_TYPE_COLOR, FILM_TYPE_BW, FILM_TYPE_VISION, FILM_TYPE_SLIDE];
export const filmSizeList = [FILM_SIZE_135MM, FILM_SIZE_120MM, FILM_SIZE_LF45, FILM_SIZE_LF810];

export const ENGLISH = 'English';
export const VIETNAMESE = 'Vietnamese';

export const laguageSelection = [
    { key: ENGLISH, label: ENGLISH },
    { key: VIETNAMESE, label: VIETNAMESE }
];

export const emailTemplate = '<h1>This is a heading</h1> <p>This is a paragraph.</p>';
export const MESSAGE_TYPE_SMS_VN = 'sms-order-film';
export const MESSAGE_TYPE_SMS_EN = 'sms-order-film-en';
