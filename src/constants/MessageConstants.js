export const buildRequiredFieldErrorMessage = (fieldName) => {
    return `Please enter your ${fieldName}!`;
};

export const selectServiceErrorMessage = 'Please select service';

export const alertTitle = 'Warning';

export const orderConfirmationTitle = 'ORDER CONFIRMATION';

export const success = 'Success';
export const failed = 'Failed';

export const loginFailed = 'Invalid email or password';

export const deleteOrderTitle = 'Delete Order?';
export const deleteOrderMessage = 'Are you sure you want to delete this order?';


