export const NAV_PUSH = 'push';
export const NAV_POP = 'pop';

export const PHONE_NUMBER_CHANGED = 'phone_number_changed';
export const NAME_CHANGED = 'name_changed';
export const EMAIL_CHANGED = 'email_changed';
export const NOTE_CHANGED = 'note_changed';
export const PROMOTE_CODE_CHANGED = 'promote_code_changed';
export const PACKAGE_CHANGED = 'package_changed';
export const QUICK_PROCESSING_CHANGED = 'quick_processing_changed';
export const BORDER_CHANGED = 'border_changed';
export const FILM_TYPE_CHANGED = 'film_type_changed';

export const FILM_ROW_CHECKED = 'film_row_changed';
export const DEV_CHECKED = 'dev_changed';
export const SCAN_CHECKED = 'scan_changed';
export const PROOFPRINT_CHECKED = 'proofprint_changed';
export const QUANTITY_CHANGED = 'quantity_changed';
export const PUSH_CHANGED = 'push_changed';
export const SUBMIT_ORDER = 'submit_order';
export const RESET_VALIDATION = 'reset_validation';
export const CONFIRM_ORDER = 'confirm_order';
export const CREATE_ORDER_SUCCESS = 'create_order_success';
export const PRICE_FETCH_SUCCESS = 'price_fetch_success';
export const PRICE_FETCHING = 'price_fetching';
export const MESSAGE_FETCH_SUCCESS = 'message_fetch_success';
export const MESSAGE_FETCHING = 'message_fetching';
export const PREPARE_DATA_SUCCESS = 'current_order_number_fetch_success';

export const SEARCH_CRITERIA_CHANGED = 'search_criteria_changed';
export const SEARCH_ORDER = 'search_order';
export const IS_SEARCHING = 'is_searching';
export const COMPLETE_SEARCH = 'complete_search';

export const PASSWORD_CHANGED = 'password_changed';
export const LOGIN_USER = 'login';
export const LOGOUT_USER = 'logout';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER_SUCCESS = 'login_user_success';

export const PENDING_ORDER_FETCHING = 'pending_order_fetching';
export const PENDING_FETCH_SUCCESS = 'pending_fetch_success';
export const COMPLETE_ORDER = 'complete_order';
export const COMPLETE_ORDER_SUCCESS = 'COMPLETE_ORDER_order_success';
export const PENDING_SEARCH_CRITERIA_CHANGED = 'pending_search_criteria_changed';
export const REMOVE_PENDING_ORDER = 'remove_pending_order';
export const REMOVE_PENDING_ORDER_SUCCESS = 'remove_pending_order_success';
export const CANCEL_ORDER = 'cancel_order';
export const CANCEL_ORDER_SUCCESS = 'cancel_order_success';

export const FETCHING_REPORTS = 'fetching_reports';
export const FETCH_REPORTS_SUCCESS = 'fetch_reports_success';
