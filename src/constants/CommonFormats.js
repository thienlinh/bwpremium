export const dateFormatRegex = '^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$';
export const phoneNumberFormatRegex = '^[0-9]{10,11}$';
export const orderNumberFormatRegex = '^[0-9]{7}$';
export const emailFormatRegex = '^.+@.+\..+';

export const dateFormat = 'DD/MM/YYYY';
export const dateTimeFormat = 'DD/MM/YYYY HH:mm';
