export const PHONE_NUMBER = 'Phone number';
export const FULL_NAME = 'Name';
export const EMAIL = 'Email';

export const DEV = 'Development';
export const SCAN = 'Scan';
export const PROOF_PRINT = 'Proofprint';
export const PUSH = 'Push';
export const QUANTITY = 'Quantity';
export const PRICE = 'Price';

export const BULLET_CHAR = '\u2022';
export const PRODUCT_SECTION_TITLE = 'Our sevices';
export const NOTE = 'Note';

export const CREATE_NEW = 'CREATE NEW ORDER';
export const SEARCH_ORDER = 'SEARCH ORDER';

export const QUANTITY_SHORT = 'QTY';
export const CURRENCY = 'VND';

export const ALL_THREE_GROUP = 'Dev, Scan & Proofprint';
export const DEV_SCAN_GROUP = 'Dev & Scan';
export const DEV_PROOFPRINT_GROUP = 'Dev & Proofprint';
export const SCAN_PROOFPRINT_GROUP = 'Scan & Proofprint';
export const DEV_GROUP = 'Dev only';
export const SCAN_GROUP = 'Scan only';
export const PROOFPRINT_GROUP = 'Proofprint only';
export const TOTAL = 'Total';

export const CONFIRM = 'CONFIRM';
export const CANCEL = 'CANCEL';
export const SUBMIT = 'SUBMIT';
export const SEARCH = 'SEARCH';

export const SEARCH_PLACEHOLDER = 'Search...';

export const PASSWORD = 'Password';
export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export const EMAIL_PLACEHOLDER = 'user@gmail.com';
export const PW_PLACEHOLDER = 'password';

export const HOME = 'HOME';

export const PENDING = 'Pending';
export const PENDING_UPPER = 'PENDING';
export const REPORT = 'Report';
export const REPORT_UPPER = 'REPORT';
export const HELLO = 'Hello';

export const DONE = 'DONE';

export const PROMOTE_CODE = 'Promote Code';

export const PACKAGE = 'EMS';
export const QUICK_PROCESSING = 'Quick Processing';
export const BORDER = 'Border';

export const FILM_TYPE = 'Film type';
export const FILM_SIZE = 'Film size';

export const FILM_SERVICE = 'Service';

export const FREE_COUNT = 'Free';

