export const IPHONE_6_SIZE = { width: 375, height: 736 };
export const IPHONE_6_PLUS_SIZE = { width: 414, height: 736 };
export const IPAD_SIZE = { width: 768, height: 1024 };

export const IPHONE_6 =
    `@media (max-device-width: ${IPHONE_6_SIZE.width}) and (max-device-height: ${IPHONE_6_SIZE.height})`;

export const IPHONE_6_PLUS =
    `@media (max-device-width: ${IPHONE_6_PLUS_SIZE.width}) and (max-device-height: ${IPHONE_6_PLUS_SIZE.height})`;

export const IPAD =
    `@media (max-device-width: ${IPAD_SIZE.width}) and (max-device-height: ${IPAD_SIZE.height})`;

export const LARGE_WIDTH =
    `@media (max-device-width: ${IPAD_SIZE.height})`;

export const MEDIUM_WIDTH =
    `@media (max-device-width: ${IPHONE_6_SIZE.height})`;

