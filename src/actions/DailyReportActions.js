import firebase from 'firebase';
import moment from 'moment';

import * as actionTypes from '../constants/ActionTypes';


export const fetchReports = () => {
    return (dispatch) => {
        (dispatch)({
            type: actionTypes.FETCHING_REPORTS
        });
        const startTime = +moment(moment().subtract(7, 'days').startOf('day'), 'X');
        firebase.database().ref('/daily-report')
            .orderByChild('createdAt')
            .startAt(startTime)
            .once('value', snapshot => {
                const result = snapshot.val();
                dispatch({
                    type: actionTypes.FETCH_REPORTS_SUCCESS,
                    payload: result ?
                        Object.keys(result).map(key => result[key]) :
                        []
                });
            });
    };
};
