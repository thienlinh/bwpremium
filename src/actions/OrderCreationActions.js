import firebase from 'firebase';
import { Alert } from 'react-native';
import moment from 'moment';
import omitBy from 'lodash.omitby';
import values from 'lodash.values';
import { email } from 'react-native-communications';

import * as actionTypes from '../constants/ActionTypes';
import * as messageConstants from '../constants/MessageConstants';
import * as valueConstants from '../constants/ValueConstants';
import * as emailTemplate from '../constants/EmailTemplate';
import * as utils from '../helpers/Utils';

export const phoneNumberChanged = (text) => (
    {
        type: actionTypes.PHONE_NUMBER_CHANGED,
        payload: text
    }
);

export const nameChanged = (text) => (
    {
        type: actionTypes.NAME_CHANGED,
        payload: text
    }
);

export const emailChanged = (text) => (
    {
        type: actionTypes.EMAIL_CHANGED,
        payload: text
    }
);

export const noteChanged = (text) => (
    {
        type: actionTypes.NOTE_CHANGED,
        payload: text
    }
);

export const promoteCodeChanged = (text) => (
    {
        type: actionTypes.PROMOTE_CODE_CHANGED,
        payload: text
    }
);

export const promteCodeChanged = (text) => (
    {
        type: actionTypes.PROMOTE_CODE_CHANGED,
        payload: text
    }
);

export const filmRowChecked = (isChecked, filmType, filmSize) => (
    {
        type: actionTypes.FILM_ROW_CHECKED,
        filmType,
        filmSize,
        payload: isChecked
    }
);

export const devChecked = (isChecked, filmType, filmSize) => (
    {
        type: actionTypes.DEV_CHECKED,
        filmType,
        filmSize,
        payload: isChecked
    }
);

export const scanChecked = (isChecked, filmType, filmSize) => (
    {
        type: actionTypes.SCAN_CHECKED,
        filmType,
        filmSize,
        payload: isChecked
    }
);

export const proofprintChecked = (isChecked, filmType, filmSize) => (
    {
        type: actionTypes.PROOFPRINT_CHECKED,
        filmType,
        filmSize,
        payload: isChecked
    }
);

export const quantityChanged = (option, filmType, filmSize) => (
    {
        type: actionTypes.QUANTITY_CHANGED,
        filmType,
        filmSize,
        payload: option
    }
);

export const pushChanged = (option) => (
    {
        type: actionTypes.PUSH_CHANGED,
        payload: option
    }
);

export const packageChanged = (type) => (
    {
        type: actionTypes.PACKAGE_CHANGED,
        payload: type
    }
);

export const quickProcessingChanged = (isChecked) => (
    {
        type: actionTypes.QUICK_PROCESSING_CHANGED,
        payload: isChecked
    }
);

export const borderChanged = (isChecked) => (
    {
        type: actionTypes.BORDER_CHANGED,
        payload: isChecked
    }
);

export const submitOrder = (order) => (
    (dispatch) => {
        const errorMessage = checkValidationBeforeSubmit(order);

        dispatch(
            {
                type: actionTypes.SUBMIT_ORDER,
                payload: { order, errorMessage }
            });

        if (errorMessage === '') {
            firebase.database().ref('/metadata/current_order_number')
                .once('value', orderNumberSnapshot => {
                    const orderNumber = calculateOrderNumber(orderNumberSnapshot.val());
                    let result = { orderNumber, count: [] };
                    firebase.database().ref(`/clients/${order.phoneNumber}`)
                        .once('value', clientSnapshot => {
                            const client = clientSnapshot.val();
                            if (utils.isDefinedAndNotNull(client)) {
                                result = {
                                    ...result,
                                    count: values(client.count)
                                };
                            }
                            dispatch(
                                {
                                    type: actionTypes.PREPARE_DATA_SUCCESS,
                                    payload: result
                                });
                        });
                });
        }
    }
);

export const resetValidation = () => (
    {
        type: actionTypes.RESET_VALIDATION
    }
);

export const confirmOrder = (order, count) => (
    (dispatch) => {
        dispatch(
            {
                type: actionTypes.CONFIRM_ORDER,
            }
        );
        const optimizedOrder = buildDataBeforeCreate(order);
        firebase.database().ref('/orders')
            .push(optimizedOrder)
            .then((orderSnapshot) => {
                firebase.database().ref(`/pending/${orderSnapshot.key}`)
                    .set(optimizedOrder);
            })
            .then(() => {
                firebase.database().ref('/metadata/current_order_number')
                    .set(optimizedOrder.orderNumber);
            })
            .then(() => {
                const { createdAt, phoneNumber, email, name } = optimizedOrder;
                const updatedAt = +moment(moment(), 'X');
                firebase.database().ref(`/clients/${optimizedOrder.phoneNumber}`)
                    .set({ createdAt, phoneNumber, email, name, updatedAt, count });
            })
            .then(() => {
                const today = +moment(moment().startOf('day'), 'X');
                firebase.database().ref('/daily-report')
                    .orderByChild('createdAt')
                    .startAt(today)
                    .once('value', snapshot => {
                        const result = snapshot.val();
                        const key = result ? Object.keys(result)[0] : null;
                        const report = handleReport(result ? result[key] : null, order);

                        let reportPromise;
                        if (utils.isNullOrUndefined(result)) {
                            reportPromise = firebase.database().ref('/daily-report')
                                .push(report);
                        } else {
                            reportPromise = firebase.database().ref(`/daily-report/${key}`)
                                .set(report);
                        }
                        reportPromise.then(() => {
                            dispatch(
                                {
                                    type: actionTypes.CREATE_ORDER_SUCCESS
                                }
                            );
                            Alert.alert(
                                messageConstants.success,
                            );

                            email(
                                [order.email],
                                null,
                                null,
                                emailTemplate.emailTitlte,
                                emailTemplate.buildEmailBody(order));
                        });
                    });
            })
            .catch(() => {
                Alert.alert(
                    messageConstants.failed
                );
            });
    }
);


export const fetchPriceData = () => {
    return (dispatch) => {
        (dispatch)({
            type: actionTypes.PRICE_FETCHING
        });
        firebase.database().ref('/price')
            .once('value', snapshot => {
                const result = snapshot.val();
                dispatch({
                    type: actionTypes.PRICE_FETCH_SUCCESS,
                    payload: result ?
                        values(result) :
                        []
                });
            });
    };
};

export const filmTypeChanged = (filmType) => (
    {
        type: actionTypes.FILM_TYPE_CHANGED,
        payload: filmType
    }
);

// export const createPrices = () => {
//     return (dispatch) => {
//         dispatch({ type: '' });

//         const filmTypeList = [
//             valueConstants.FILM_TYPE_COLOR,
//             valueConstants.FILM_TYPE_BW,
//             valueConstants.FILM_TYPE_VISION,
//             valueConstants.FILM_TYPE_SLIDE
//         ];

//         const filmSizeList = [
//             valueConstants.FILM_SIZE_135MM,
//             valueConstants.FILM_SIZE_120MM,
//             valueConstants.FILM_SIZE_LF45,
//             valueConstants.FILM_SIZE_LF810,
//         ];

//         for (const type of filmTypeList) {
//             for (const size of filmSizeList) {
//                 let order = {
//                     filmSize: size,
//                     filmType: type,
//                     bothPrice: 50000,
//                     devPrice: 30000,
//                     scanPrice: 30000,
//                     pushPrice: 10000,
//                     quickProcessingPrice: 20000
//                 };
//                 if (type === valueConstants.FILM_TYPE_BW) {
//                     order = { ...order, proofprintPrice: 30000, allThreePrice: 100000 };
//                 }
//                 firebase.database().ref('/price')
//                     .push(order)
//                     .then(() => { dispatch({ type: '' }); });
//             }
//         }
//     };
// };

const buildDataBeforeCreate = (order) => {
    let optimizedOrder = order;
    optimizedOrder.createdAt = +moment(moment(), 'X');

    const productList = [];
    for (let product of optimizedOrder.products) {
        product = omitBy(product, i => utils.isNullOrUndefined(i));
        delete product.rowChecked;
        productList.push(product);
    }
    optimizedOrder = { ...optimizedOrder, products: productList };
    return optimizedOrder;
};

const calculateOrderNumber = (orderNumber) => {
    const oldDate = moment(orderNumber.substr(0, 4), 'DDMM');
    if (oldDate.startOf('day') < moment().startOf('day')) {
        return moment().format('DDMM000');
    }
    const orderNumberInt = +orderNumber + 1;
    return `${orderNumberInt}`;
};

const checkValidationBeforeSubmit = (order) => {
    let errorMessage = '';
    if (!order.phoneNumber) {
        errorMessage = messageConstants.buildRequiredFieldErrorMessage('phone number');
    } else if (!order.name) {
        errorMessage = messageConstants.buildRequiredFieldErrorMessage('name');
    } else if (!order.email) {
        errorMessage = messageConstants.buildRequiredFieldErrorMessage('email');
    } else {
        const productList = order.products;
        const isValid = productList.some(i => i.rowChecked && i.quantity > 0);

        if (!isValid) {
            errorMessage = messageConstants.selectServiceErrorMessage;
        }
    }
    return errorMessage;
};

const handleReport = (report, order) => {
    const now = +moment(moment(), 'X');
    if (utils.isNullOrUndefined(report)) {
        report = {
            createdAt: now,
            updatedAt: now,
            total: order.totalPrice,
            detail: []
        };
    } else {
        report = {
            ...report,
            updatedAt: now,
            total: report.total += order.totalPrice
        };
    }

    let group = report.detail.find(g => g.type === order.type);

    if (utils.isNullOrUndefined(group)) {
        group = { type: order.type, list: [], total: 0 };
        report.detail.push(group);
    }

    group.total += order.totalPrice;

    order.products.forEach(product => {
        group.list = createOrUpdateReport(group.list, product);
    });


    return report;
};

const createOrUpdateReport = (detailList, product) => {
    if (utils.isNullOrUndefined(detailList)) {
        detailList = [];
    }
    const detailItem = detailList.find(d => d.filmType === product.filmType &&
        d.filmSize === product.filmSize &&
        d.dev === product.dev &&
        d.scan === product.scan &&
        d.proofprint === product.proofprint);

    if (utils.isNullOrUndefined(detailItem)) {
        let item = {
            filmType: product.filmType,
            filmSize: product.filmSize,
            quantity: product.quantity,
            price: product.price,
            dev: product.dev,
            scan: product.scan,
        };
        if (product.filmType === valueConstants.FILM_TYPE_BW) {
            item = { ...item, proofprint: product.proofprint };
        }
        detailList.push(item);
    } else {
        detailItem.quantity += product.quantity;
        detailItem.price += product.price;
    }

    return detailList;
};
