import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

import * as actionTypes from '../constants/ActionTypes';

export const authEmailChanged = (text) => {
    return {
        type: actionTypes.EMAIL_CHANGED,
        payload: text
    };
};

export const passwordChanged = (text) => {
    return {
        type: actionTypes.PASSWORD_CHANGED,
        payload: text
    };
};

export const loginUser = ({ email, password }) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.LOGIN_USER });

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(user => loginUserSuccess(dispatch, user))
            .catch(() => {
                loginUserFail(dispatch);
            });
    };
};

export const logout = () => {
    return (dispatch) => {
        dispatch({ type: actionTypes.LOGOUT_USER });
        firebase.auth().signOut()
            .then(() => Actions.login({ type: 'reset' }));
    };
};

const loginUserFail = (dispatch) => {
    dispatch({ type: actionTypes.LOGIN_USER_FAIL });
};

const loginUserSuccess = (dispatch, user) => {
    dispatch({
        type: actionTypes.LOGIN_USER_SUCCESS,
        payload: user
    });

    Actions.menu({ type: 'reset' });
};
