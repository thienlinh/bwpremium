import firebase from 'firebase';

import * as actionTypes from '../constants/ActionTypes';

export const pendingSearchCriteriaChanged = (criteria) => (
    {
        type: actionTypes.PENDING_SEARCH_CRITERIA_CHANGED,
        payload: criteria
    }
);

export const fetchPendingOrder = (isFirstLoad) => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.PENDING_ORDER_FETCHING,
            payload: isFirstLoad
        });

        firebase.database().ref('/pending')
            .once('value', snapshot => {
                const result = snapshot.val();
                dispatch({
                    type: actionTypes.PENDING_FETCH_SUCCESS,
                    payload: result ?
                        Object.keys(result).map(key => ({ ...result[key], key })) :
                        []
                });
            });
    };
};


export const donePendingOrder = (order, language) => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.COMPLETE_ORDER
        });

        firebase.database().ref(`/pending/${order.key}`)
            .remove(() => {
                dispatch({
                    type: actionTypes.COMPLETE_ORDER_SUCCESS,
                    language,
                    payload: order
                });
            });
    };
};

export const deletePendingOrder = (order) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.REMOVE_PENDING_ORDER });

        firebase.database().ref(`/pending/${order.key}`)
            .remove(() => {
                firebase.database().ref(`/orders/${order.key}`)
                    .remove(() => {
                        // TODO: edit daily report
                        dispatch({
                            type: actionTypes.REMOVE_PENDING_ORDER_SUCCESS,
                            payload: order.key
                        });
                    });
            });
    };
};

export const fetchMessageData = () => {
    return (dispatch) => {
        (dispatch)({
            type: actionTypes.MESSAGE_FETCHING
        });
        firebase.database().ref('/messages')
            .once('value', snapshot => {
                const result = snapshot.val();
                dispatch({
                    type: actionTypes.MESSAGE_FETCH_SUCCESS,
                    payload: result ?
                        Object.keys(result).map(key => result[key]) :
                        []
                });
            });
    };
};
