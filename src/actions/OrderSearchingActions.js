import firebase from 'firebase';
import moment from 'moment';

import * as actionTypes from '../constants/ActionTypes';
import * as commonFormats from '../constants/CommonFormats';

export const searchCriteriaChanged = (criteria) => (
    {
        type: actionTypes.SEARCH_CRITERIA_CHANGED,
        payload: criteria
    }
);

export const searchOrder = (criteria) => (
    (dispatch) => {
        dispatch(
            {
                type: actionTypes.IS_SEARCHING,
            });

        const searchType = criteria.match(commonFormats.dateFormatRegex)
            ? 'createdAt'
            : criteria.match(commonFormats.phoneNumberFormatRegex)
                ? 'phoneNumber'
                : criteria.match(commonFormats.orderNumberFormatRegex)
                    ? 'orderNumber'
                    : criteria.match(commonFormats.emailFormatRegex)
                        ? 'email'
                        : 'name';

        if (searchType === 'createdAt') {
            const targetDate = moment(criteria, commonFormats.dateFormat);
            const startTime = +moment(targetDate, 'x');
            const endTime = +moment(targetDate.add(1, 'd'), 'x');

            firebase.database().ref('/orders')
                .orderByChild(searchType)
                .startAt(startTime)
                .endAt(endTime)
                .once('value', snapshot => {
                    const result = snapshot.val();
                    dispatch({
                        type: actionTypes.COMPLETE_SEARCH,
                        payload: result ?
                            Object.keys(result).map(key => result[key]) :
                            []
                    });
                });
        } else {
            firebase.database().ref('/orders')
                .orderByChild(searchType)
                .equalTo(criteria)
                .once('value', snapshot => {
                    const result = snapshot.val();
                    dispatch({
                        type: actionTypes.COMPLETE_SEARCH,
                        payload: result ?
                            Object.keys(result).map(key => result[key]) :
                            []
                    });
                });
        }
    }
);
