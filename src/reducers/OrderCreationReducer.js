import * as actionTypes from '../constants/ActionTypes';
import * as valueConstants from '../constants/ValueConstants';
import * as utils from '../helpers/Utils';

const INITIAL_STATE = {
    errorMessage: '',
    isValid: false,
    isLoading: false,
    popupShowing: false,
    currentFilmType: valueConstants.FILM_TYPE_COLOR,
    order: {
        phoneNumber: '',
        name: '',
        email: '',
        note: '',
        totalPrice: 0,
        status: valueConstants.ORDER_STATUS_PENDING,
        orderNumber: '',
        promoteCode: '',
        discountPrice: 1,
        type: valueConstants.ORDER_TYPE_DIRECT,
        border: false,
        quickProcessing: false,
        freeCount: 0,
        freePrice: 0,
        push: 0,
        products: []
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case actionTypes.PHONE_NUMBER_CHANGED:
            return { ...state, order: { ...state.order, phoneNumber: action.payload } };
        case actionTypes.NAME_CHANGED:
            return { ...state, order: { ...state.order, name: action.payload } };
        case actionTypes.EMAIL_CHANGED:
            return { ...state, order: { ...state.order, email: action.payload } };
        case actionTypes.NOTE_CHANGED:
            return { ...state, order: { ...state.order, note: action.payload } };
        case actionTypes.PROMOTE_CODE_CHANGED:
            return { ...state, order: { ...state.order, promoteCode: action.payload } };
        case actionTypes.FILM_ROW_CHECKED:
            {
                const products = action.payload ?
                    [
                        ...state.order.products,
                        {
                            rowChecked: action.payload,
                            dev: action.payload,
                            scan: action.payload,
                            proofprint: action.filmType === valueConstants.FILM_TYPE_BW ?
                                action.payload :
                                undefined,
                            quantity: action.payload ? 1 : 0,
                            filmType: action.filmType,
                            filmSize: action.filmSize,
                            price: 0
                        }
                    ] :
                    state.order.products.filter(p => !(p.filmSize === action.filmSize
                        && p.filmType === action.filmType));
                return {

                    ...state,
                    order: {
                        ...state.order,
                        products
                    }
                };
            }
        case actionTypes.DEV_CHECKED:
            {
                let products = [...state.order.products];

                const product = products.find(p => p.filmType === action.filmType &&
                    p.filmSize === action.filmSize);

                if (product !== undefined) {
                    const isRowChecked = action.payload ||
                        product.scan ||
                        (action.filmType === valueConstants.FILM_TYPE_BW && product.proofprint);

                    if (isRowChecked) {
                        product.dev = action.payload;
                    } else {
                        products = products.filter(p => !(p.filmSize === action.filmSize
                            && p.filmType === action.filmType));
                    }
                } else if (action.payload) {
                    products = [
                        ...products,
                        {
                            rowChecked: true,
                            dev: true,
                            scan: false,
                            proofprint: action.filmType === valueConstants.FILM_TYPE_BW ?
                                false :
                                undefined,
                            quantity: 1,
                            filmType: action.filmType,
                            filmSize: action.filmSize,
                            price: 0
                        }
                    ];
                }

                return {
                    ...state,
                    order: {
                        ...state.order,
                        products
                    }
                };
            }
        case actionTypes.SCAN_CHECKED:
            {
                let products = [...state.order.products];

                const product = products.find(p => p.filmType === action.filmType &&
                    p.filmSize === action.filmSize);

                if (product !== undefined) {
                    const isRowChecked = action.payload ||
                        product.dev ||
                        (action.filmType === valueConstants.FILM_TYPE_BW && product.proofprint);

                    if (isRowChecked) {
                        product.scan = action.payload;
                    } else {
                        products = products.filter(p => !(p.filmSize === action.filmSize
                            && p.filmType === action.filmType));
                    }
                } else if (action.payload) {
                    products = [
                        ...products,
                        {
                            rowChecked: true,
                            dev: false,
                            scan: true,
                            proofprint: action.filmType === valueConstants.FILM_TYPE_BW ?
                                false :
                                undefined,
                            quantity: 1,
                            filmType: action.filmType,
                            filmSize: action.filmSize,
                            price: 0
                        }
                    ];
                }

                return {
                    ...state,
                    order: {
                        ...state.order,
                        products
                    }
                };
            }
        case actionTypes.PROOFPRINT_CHECKED:
            {
                let products = [...state.order.products];

                const product = products.find(p => p.filmType === action.filmType &&
                    p.filmSize === action.filmSize);

                if (product !== undefined) {
                    const isRowChecked = (action.filmType === valueConstants.FILM_TYPE_BW &&
                        action.payload) ||
                        product.dev ||
                        product.scan;

                    if (isRowChecked) {
                        product.proofprint = action.filmType === valueConstants.FILM_TYPE_BW &&
                            action.payload;
                    } else {
                        products = products.filter(p => !(p.filmSize === action.filmSize
                            && p.filmType === action.filmType));
                    }
                } else if (action.payload) {
                    products = [
                        ...products,
                        {
                            rowChecked: true,
                            dev: false,
                            scan: false,
                            proofprint: action.filmType === valueConstants.FILM_TYPE_BW &&
                                action.payload,
                            quantity: 1,
                            filmType: action.filmType,
                            filmSize: action.filmSize,
                            price: 0
                        }
                    ];
                }

                return {
                    ...state,
                    order: {
                        ...state.order,
                        products
                    }
                };
            }
        case actionTypes.QUANTITY_CHANGED:
            {
                let products = [...state.order.products];

                const product = products.find(p => p.filmType === action.filmType &&
                    p.filmSize === action.filmSize);

                if (product !== undefined) {
                    product.quantity = action.payload;
                } else {
                    products = [
                        ...products,
                        {
                            rowChecked: true,
                            dev: true,
                            scan: true,
                            proofprint: action.filmType === valueConstants.FILM_TYPE_BW,
                            quantity: action.payload,
                            filmType: action.filmType,
                            filmSize: action.filmSize,
                            price: 0
                        }
                    ];
                }

                return {
                    ...state,
                    order: {
                        ...state.order,
                        products
                    }
                };
            }
        case actionTypes.SUBMIT_ORDER:
            {
                const errorMessage = action.payload.errorMessage;
                if (errorMessage !== '') {
                    return { ...state, errorMessage };
                }

                return { ...state, order: { ...action.payload.order }, isLoading: true };
            }
        case actionTypes.RESET_VALIDATION:
            return { ...state, errorMessage: '', isValid: false };
        case actionTypes.CONFIRM_ORDER:
            return { ...state, isLoading: true, isValid: false, popupShowing: false };
        case actionTypes.CREATE_ORDER_SUCCESS:
            return { ...INITIAL_STATE, price: [...state.price] };
        case actionTypes.PRICE_FETCHING:
            return { ...state, isLoading: true };
        case actionTypes.PRICE_FETCH_SUCCESS:
            return { ...state, price: [...action.payload], isLoading: false };
        case actionTypes.PREPARE_DATA_SUCCESS:
            buildPriceBeforeSubmit(state.order, state.price, action.payload.count);
            return {
                ...state,
                isLoading: false,
                isValid: true,
                popupShowing: true,
                count: action.payload.count,
                order: {
                    ...state.order,
                    orderNumber: action.payload.orderNumber,
                }
            };
        case actionTypes.PUSH_CHANGED:
            return { ...state, order: { ...state.order, push: action.payload } };
        case actionTypes.PACKAGE_CHANGED:
            return { ...state, order: { ...state.order, type: action.payload } };
        case actionTypes.BORDER_CHANGED:
            return { ...state, order: { ...state.order, border: action.payload } };
        case actionTypes.QUICK_PROCESSING_CHANGED:
            return { ...state, order: { ...state.order, quickProcessing: action.payload } };
        case actionTypes.FILM_TYPE_CHANGED:
            return { ...state, currentFilmType: action.payload };
        default:
            return state;
    }
};


const buildPriceBeforeSubmit = (order, priceList, countList) => {
    order.totalPrice = 0;
    order.freeCount = 0;
    order.freePrice = 0;
    order.products.forEach(product => {
        let count = countList.find(p => p.filmType === product.filmType &&
            p.filmSize === product.filmSize);

        if (utils.isDefinedAndNotNull(count)) {
            count.number += product.quantity;
        } else {
            count = {
                filmType: product.filmType,
                filmSize: product.filmSize,
                number: product.quantity,
                freeCount: 0
            };
            countList.push(count);
        }

        const price = priceList.find(p => p.filmType === product.filmType &&
            p.filmSize === product.filmSize);

        if (product.filmType === valueConstants.FILM_TYPE_BW &&
            product.dev && product.scan
            && product.proofprint) {
            product.price = price.allThreePrice * product.quantity;
        } else if (product.dev && product.scan) {
            product.price = price.bothPrice * product.quantity;
        } else {
            product.price = (
                product.dev ? price.devPrice : 0
                    + product.scan ? price.scanPrice : 0
                        + product.proofprint ? price.proofprintPrice : 0)
                * product.quantity;
        }
        order.totalPrice += product.price;

        if (Math.floor(count.number / 11) > count.freeCount) {
            order.freeCount = Math.floor(count.number / 11) - count.freeCount;
            count.freeCount = Math.floor(count.number / 11);
            order.freePrice = (product.price / product.quantity) * order.freeCount;
            order.totalPrice -= order.freePrice;
        }
    });
};

