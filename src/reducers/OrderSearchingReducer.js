
import orderBy from 'lodash.orderby';
import * as actionTypes from '../constants/ActionTypes';

const INITIAL_STATE = {
    criteria: '',
    isLoading: false,
    data: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.SEARCH_CRITERIA_CHANGED:
            return { ...state, criteria: action.payload };
        case actionTypes.IS_SEARCHING:
            return { ...state, isLoading: true };
        case actionTypes.COMPLETE_SEARCH:
            return {
                ...state,
                isLoading: false,
                data: [...orderBy(action.payload, ['createdAt'], ['desc'])]
            };
        default:
            return state;
    }
};
