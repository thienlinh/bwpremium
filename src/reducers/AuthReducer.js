import * as actionTypes from '../constants/ActionTypes';
import * as messageConstants from '../constants/MessageConstants';

const INITIAL_STATE = {
    email: '',
    password: '',
    user: {},
    loading: false,
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case actionTypes.EMAIL_CHANGED:
            return { ...state, email: action.payload };
        case actionTypes.PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case actionTypes.LOGIN_USER:
            return { ...state, loading: true, error: '' };
        case actionTypes.LOGIN_USER_SUCCESS:
            return { ...state, ...INITIAL_STATE, user: action.payload };
        case actionTypes.LOGIN_USER_FAIL:
            return { ...state, error: messageConstants.loginFailed, password: '', loading: false };
        case actionTypes.LOGOUT_USER:
            return INITIAL_STATE;
        default:
            return state;

    }
};
