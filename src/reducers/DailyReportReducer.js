import * as actionTypes from '../constants/ActionTypes';
import * as valueConstants from '../constants/ValueConstants';

const INITIAL_STATE = {
    reportList: [],
    isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.FETCHING_REPORTS:
            return { ...state, isLoading: true };
        case actionTypes.FETCH_REPORTS_SUCCESS:
            return { ...state, reportList: action.payload, isLoading: false };
        default:
            return state;
    }
};
