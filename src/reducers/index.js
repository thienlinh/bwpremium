import { combineReducers } from 'redux';

import orderCreation from './OrderCreationReducer';
import orderSearching from './OrderSearchingReducer';
import auth from './AuthReducer';
import pending from './PendingReducer';
import dailyReport from './DailyReportReducer';

export default combineReducers({
    orderCreation,
    orderSearching,
    auth,
    pending,
    dailyReport
});
