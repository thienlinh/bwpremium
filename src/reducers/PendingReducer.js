
import moment from 'moment';
import { text } from 'react-native-communications';
import template from 'lodash.template';
import orderBy from 'lodash.orderby';

import * as actionTypes from '../constants/ActionTypes';
import * as commonFormats from '../constants/CommonFormats';
import * as valueConstants from '../constants/ValueConstants';

const INITIAL_STATE = {
    pendingSearchCriteria: '',
    pendingList: [],
    originalPendingList: [],
    messages: [],
    isLoading: false,
    firstLoad: false,
    isFetchingMessages: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.PENDING_SEARCH_CRITERIA_CHANGED:
            {
                const pendingSearchCriteria = action.payload;
                let pendingList;

                if (pendingSearchCriteria.match(commonFormats.dateFormatRegex)) {
                    const targetDate = moment(pendingSearchCriteria, commonFormats.dateFormat);
                    const startTime = +moment(targetDate, 'x');
                    const endTime = +moment(targetDate.add(1, 'd'), 'x');

                    pendingList =
                        state.originalPendingList.filter(item =>
                            item.createdAt >= startTime && item.createdAt <= endTime);
                } else {
                    pendingList =
                        state.originalPendingList.filter(item =>
                            item.name.includes(pendingSearchCriteria)
                            || item.phoneNumber.includes(pendingSearchCriteria)
                            || item.email.includes(pendingSearchCriteria)
                            || item.orderNumber.includes(pendingSearchCriteria));
                }
                return { ...state, pendingList, pendingSearchCriteria };
            }
        case actionTypes.PENDING_ORDER_FETCHING:
            return action.payload ? { ...state, firstLoad: true } : { ...state, isLoading: true };
        case actionTypes.PENDING_FETCH_SUCCESS:
            {
                const list = orderBy(action.payload, ['createdAt'], ['desc']);
                return {
                    ...state,
                    originalPendingList: list,
                    pendingList: list,
                    firstLoad: false,
                    isLoading: false,
                    pendingSearchCriteria: ''
                };
            }
        case actionTypes.COMPLETE_ORDER:
        case actionTypes.REMOVE_PENDING_ORDER:
            return { ...state, isLoading: true };
        case actionTypes.COMPLETE_ORDER_SUCCESS:
            {
                sendSms(action.payload, action.language, state.messages);

                const pendingList =
                    state.originalPendingList.filter(item => item.key !== action.payload.key);
                return { ...state, pendingList, isLoading: false };
            }
        case actionTypes.REMOVE_PENDING_ORDER_SUCCESS:
            {
                const pendingList =
                    state.originalPendingList.filter(item => item.key !== action.payload);
                return { ...state, pendingList, isLoading: false };
            }
        case actionTypes.MESSAGE_FETCHING:
            return { ...state, isFetchingMessages: true };
        case actionTypes.MESSAGE_FETCH_SUCCESS:
            return { ...state, messages: [...action.payload], isFetchingMessages: false };
        case actionTypes.PREPARE_DATA_SUCCESS:
        default:
            return state;
    }
};

const sendSms = (order, language, messageList) => {
    let msg;
    switch (language) {
        case valueConstants.VIETNAMESE:
            msg = messageList.find(m => m.type === valueConstants.MESSAGE_TYPE_SMS_VN);
            break;
        case valueConstants.ENGLISH:
            msg = messageList.find(m => m.type === valueConstants.MESSAGE_TYPE_SMS_EN);
            break;
        default:
            msg = messageList.find(m => m.type === valueConstants.MESSAGE_TYPE_SMS_VN);
    }
    const formattedMsg = template(msg.content)({
        CLIENT_NAME: order.name,
        ORDER_NUMBER: order.orderNumber,
        CANCELED_DATE: moment().add(15, 'day').format(commonFormats.dateFormat)
    });
    text(order.phoneNumber, formattedMsg);
};

