import React, { Component } from 'react';
import { Provider } from 'react-redux';
//import { Examples, View } from '@shoutem/ui';
import { createStore, applyMiddleware } from 'redux';
import SplashScreen from 'react-native-splash-screen';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import reducers from './reducers';

import TabNavigation from './components/TabNavigation';


class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyCG5JxgJe-5axF5tF2hYLDTi868NKyXMLM',
            authDomain: 'llab2-d1083.firebaseapp.com',
            databaseURL: 'https://llab2-d1083.firebaseio.com',
            projectId: 'llab2-d1083',
            storageBucket: 'llab2-d1083.appspot.com',
            messagingSenderId: '327514193065'
        };
        firebase.initializeApp(config);
    }
    componentDidMount() {
        // do stuff while splash screen is shown
        // After having done stuff (such as async tasks) hide the splash screen
        //setTimeout(() => SplashScreen.hide(), 10000);
        SplashScreen.hide();
    }

    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

        return (
            //<Examples />
            <Provider store={store}>
                <TabNavigation />
            </Provider>

        );
    }
}

export default App;
