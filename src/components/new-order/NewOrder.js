import React, { Component } from 'react';
import {
    Keyboard,
    Alert,
    TouchableWithoutFeedback,
    ScrollView,
    View
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import PopupDialog, { ScaleAnimation, DialogTitle } from 'react-native-popup-dialog';
import { MediaQueryStyleSheet, MediaQuery } from 'react-native-responsive';

import * as colors from '../../style/colors';
import ClientInfoSection from './client-info-section/ClientInfoSection';
import ProductSelectionSection from './ProductSelectionSection';
import NoteSection from './note-section/NoteSection';
import OptionalChoice from './optional-choice/OptionalChoice';
import { AppButton, Header, Spinner, InputField } from '../commons';
import * as titleConstants from '../../constants/TitleConstants';
import * as messageConstants from '../../constants/MessageConstants';
import {
    submitOrder,
    resetValidation,
    fetchPriceData,
    fetchCurrentOrderNumber,
    promoteCodeChanged,
    //createPrices
} from '../../actions';
import OrderConfirmation from './order-confirmation/OrderConfirmation';
import {
    IPAD_SIZE,
    IPHONE_6_PLUS,
    IPHONE_6_PLUS_SIZE,
} from '../../style/breakpoints';
import * as utils from '../../helpers/Utils';

class NewOrder extends Component {

    static navigationOptions = {
        tabBarLabel: 'New Order',
        tabBarIcon: () => (<Icon size={24} color="white" name="plus-circle" />)
    };

    componentWillMount() {
        this.props.fetchPriceData();
    }


    componentDidUpdate() {
        const { popupShowing, isValid } = this.props;
        if (this.props.errorMessage !== '') {
            Alert.alert(
                messageConstants.alertTitle,
                this.props.errorMessage,
                [
                    { text: 'OK', onPress: () => this.props.resetValidation() },
                ]
            );
        } else if (isValid && popupShowing) {
            this.popupDialog.show();
        } else if (utils.isDefinedAndNotNull(this.popupDialog) &&
            this.popupDialog.dialog.state.dialogState === 'opened') {
            this.popupDialog.dismiss();
        }
    }


    onSubmitButtonPress() {
        this.props.submitOrder(this.props.order);
    }

    onChangePromoteCode(text) {
        this.props.promoteCodeChanged(text);
    }

    renderSpinner() {
        const { isLoading, isFetchingPrice, isFetchingMessages } = this.props;
        if (isLoading || isFetchingPrice || isFetchingMessages) {
            return (<Spinner />);
        }
    }

    renderPopup(width, height) {
        const { count, order, popupShowing } = this.props;
        return (
            popupShowing &&
            <PopupDialog
                ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                dialogAnimation={new ScaleAnimation()}
                width={width}
                height={height}
                onDismissed={() => this.props.resetValidation()}
                dialogTitle={
                    <DialogTitle
                        title={messageConstants.orderConfirmationTitle}
                        titleStyle={{ backgroundColor: 'black' }}
                        titleTextStyle={{ color: 'white' }}
                    />}
            >
                <OrderConfirmation
                    count={count}
                    order={order}
                    popupDialog={this.popupDialog}
                />
            </PopupDialog>

        );
    }


    render() {
        return (

            <ScrollView
                showsVerticalScrollIndicator
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                    <View>
                        <Header title={titleConstants.CREATE_NEW} />

                        <KeyboardAwareScrollView
                            style={{ backgroundColor: colors.lightGray }}
                            resetScrollToCoords={{ x: 0, y: 0 }}
                            contentContainerStyle={styles.container}
                            scrollEnabled={false}

                        >
                            <View style={styles.containerStyle}>

                                <View style={styles.topContentStyle}>
                                    <ClientInfoSection
                                        style={{
                                            flexDirection: 'column',
                                            marginBottom: 20,
                                            flex: 2
                                        }}
                                    />
                                    <InputField
                                        style={{ flex: 1, marginLeft: 40 }}
                                        label={titleConstants.PROMOTE_CODE}
                                        onChangeText={this.onChangePromoteCode.bind(this)}
                                        value={this.props.promoteCode}
                                    />
                                </View>

                                <ProductSelectionSection />

                                <View style={styles.bottomContentStyle}>
                                    <NoteSection
                                        style={{ flex: 1 }}
                                    />
                                    <View
                                        style={styles.sumitContainerStyle}
                                    >
                                        <OptionalChoice />
                                        <MediaQuery
                                            maxDeviceWidth={IPHONE_6_PLUS_SIZE.width}
                                            maxDeviceHeight={IPHONE_6_PLUS_SIZE.height}
                                        >
                                            <AppButton
                                                style={{ width: 100, marginTop: 10 }}
                                                text={titleConstants.SUBMIT}
                                                onPress={this.onSubmitButtonPress.bind(this)}
                                            //onPress={() => this.props.createPrices()}
                                            />
                                        </MediaQuery>

                                        <MediaQuery
                                            minDeviceWidth={IPAD_SIZE.width}
                                            minDeviceHeight={IPAD_SIZE.height}
                                        >
                                            <AppButton
                                                style={{ width: 250, marginTop: 10 }}
                                                text={titleConstants.SUBMIT}
                                                onPress={this.onSubmitButtonPress.bind(this)}
                                            //onPress={() => this.props.createPrices()}
                                            />
                                        </MediaQuery>
                                    </View>
                                </View>

                            </View>
                        </KeyboardAwareScrollView>

                        <MediaQuery
                            maxDeviceWidth={IPHONE_6_PLUS_SIZE.width}
                            maxDeviceHeight={IPHONE_6_PLUS_SIZE.height}
                        >
                            {this.renderPopup(0.9, 0.9)}
                        </MediaQuery>

                        <MediaQuery
                            minDeviceWidth={IPAD_SIZE.width}
                            minDeviceHeight={IPAD_SIZE.height}
                        >
                            {this.renderPopup(0.6, 0.8)}
                        </MediaQuery>

                        {this.renderSpinner()}

                    </View>

                </TouchableWithoutFeedback>
            </ScrollView>

        );
    }
}

const styles = MediaQueryStyleSheet.create({
    containerStyle: {
        flexDirection: 'column',
        flex: 1,
        padding: 30,
        backgroundColor: colors.lightGray,
        justifyContent: 'space-around'
    },
    topContentStyle: {
        flexDirection: 'row'
    },
    blockStyle: {
        flexDirection: 'column',
        marginBottom: 20
    },
    sumitContainerStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    bottomContentStyle: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
},
    {
        [IPHONE_6_PLUS]: {
            containerStyle: {
                padding: 5
            },
            sumitContainerStyle: {
                marginTop: 30,
            },
        }
    }
);

const mapStateToProps = ({ orderCreation }) => {
    const {
        errorMessage,
        order,
        count,
        isValid,
        isLoading,
        promoteCode,
        isFetchingPrice,
        isFetchingMessages,
        popupShowing
         } = orderCreation;
    return {
        errorMessage,
        order,
        count,
        isValid,
        isLoading,
        promoteCode,
        isFetchingPrice,
        isFetchingMessages,
        popupShowing
    };
};

export default connect(mapStateToProps, {
    submitOrder,
    resetValidation,
    fetchPriceData,
    fetchCurrentOrderNumber,
    promoteCodeChanged,
    //createPrices
})(NewOrder);
