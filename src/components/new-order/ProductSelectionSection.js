import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from '@shoutem/ui';
import SegmentTab from 'react-native-segment-tab';
import { Dimensions } from 'react-native';

import ProductSelectionPage from '../new-order/product-selection-page/ProductSelectionPage';

import * as valueConstants from '../../constants/ValueConstants';
import * as tileConstants from '../../constants/TitleConstants';
import { SectionTitle } from '../commons';
import {
    filmTypeChanged
} from '../../actions';

const { width } = Dimensions.get('window');

class ProductSelectionSection extends Component {
    onChangeFilmType(index) {
        this.props.filmTypeChanged(valueConstants.filmTypeList[index]);
    }

    render() {
        const { currentFilmType, products } = this.props;

        return (
            <View
                style={styles.container}
                styleName="vertical"
            >
                <SectionTitle>
                    {`${tileConstants.BULLET_CHAR} ${tileConstants.PRODUCT_SECTION_TITLE}`}
                </SectionTitle>

                <SegmentTab
                    data={valueConstants.filmTypeList}
                    activeColor='black'
                    horizontalWidth={width * 0.9}
                    titleSize={16}
                    selected={valueConstants.filmTypeList.indexOf(currentFilmType)}
                    style={{ marginBottom: 10 }}
                    onPress={this.onChangeFilmType.bind(this)}
                />
                <ProductSelectionPage
                    productPageStyle={{ ...styles.blockStyle, height: 200 }}
                    filmType={currentFilmType}
                    isBw={currentFilmType === valueConstants.FILM_TYPE_BW}
                    products={products !== undefined ?
                        products.filter(p => p.filmType === currentFilmType) :
                        []}
                />
            </View>
        );
    }
}

const styles = {
    container: {

    }
};

const mapStateToProps = ({ orderCreation }) => {
    const { currentFilmType } = orderCreation;
    const { products } = orderCreation.order;

    return { currentFilmType, products };
};

export default connect(mapStateToProps, {
    filmTypeChanged
})(ProductSelectionSection);
