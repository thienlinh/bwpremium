import React, { Component } from 'react';
import { View, Text } from '@shoutem/ui';
import { connect } from 'react-redux';
import { Dimensions } from 'react-native';

import * as tileConstants from '../../../constants/TitleConstants';
import { SectionTitle, AppButton } from '../../commons';
import ProductInformation from './product-information/ProductInformation';
import { confirmOrder } from '../../../actions';
import * as utils from '../../../helpers/Utils';

const { height } = Dimensions.get('window');

class OrderConfirmation extends Component {
    componentDidUpdate() {

    }

    onConfirmOrder() {
        const { order, count } = this.props;
        this.props.confirmOrder(order, count);
        //this.props.popupDialog.dismiss();
    }

    onDismissPopup() {
        if (utils.isDefinedAndNotNull(this.props.popupDialog)) {
            this.props.popupDialog.dismiss();
        }
    }


    renderOrderedProducts() {
        return (
            <ProductInformation
                order={this.props.order}
            />
        );
    }

    render() {
        const { phoneNumber, name, email, note, orderNumber } = this.props.order;
        return (
            <View
                style={styles.containerStyle}
                styleName="fill-parent vertical"
            >

                <View style={{ ...styles.sectionStyle, marginLeft: 20 }}>
                    <SectionTitle
                        fontWeight='400'
                        fontSize={16}
                    >
                        {orderNumber}
                    </SectionTitle>
                    <Text>
                        {phoneNumber}
                    </Text>
                    <Text>
                        {name}
                    </Text>
                    <Text>
                        {email}
                    </Text>
                </View>

                <View
                    style={styles.sectionStyle}

                >
                    <SectionTitle>
                        {`${tileConstants.BULLET_CHAR} ${tileConstants.PRODUCT_SECTION_TITLE}`}
                    </SectionTitle>

                    {this.renderOrderedProducts()}

                </View>

                <View
                    style={styles.sectionStyle}
                >

                    <SectionTitle>
                        {`${tileConstants.BULLET_CHAR} ${tileConstants.NOTE}`}
                    </SectionTitle>

                    <Text style={{ marginLeft: 20 }}>
                        {note}
                    </Text>
                </View>

                <View
                    style={styles.sectionStyle}
                    styleName="horizontal flexible h-end"
                >
                    <AppButton
                        text={tileConstants.CONFIRM}
                        style={styles.buttonStyle}
                        onPress={this.onConfirmOrder.bind(this)}
                    />
                    <AppButton
                        text={tileConstants.CANCEL}
                        type="secondary"
                        style={styles.buttonStyle}
                        onPress={this.onDismissPopup.bind(this)}
                    />
                </View>

            </View>
        );
    }
}

const styles = {
    containerStyle: {
        padding: 10,
        marginTop: 30
    },
    sectionStyle: {
        marginTop: height * 0.02,
    },
    buttonStyle: {
        marginLeft: 5,
        width: 150,
        minHeight: 30
    }
};

export default connect(null, { confirmOrder })(OrderConfirmation);

