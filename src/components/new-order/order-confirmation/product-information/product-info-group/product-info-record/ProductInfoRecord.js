import React, { Component } from 'react';
import { View, Text } from 'react-native';

import ProductItemStyle from '../../ProductItemStyle';

class ProductInfoRecord extends Component {
    render() {
        const { product } = this.props;
        return (
            <View style={ProductItemStyle.recordStyle}>
                <Text style={ProductItemStyle.filmNameItemStyle}>
                    {product.title ? product.title : `${product.filmSize} ${product.filmType}`}
                </Text>
                <Text style={ProductItemStyle.itemStyle}>
                    {product.quantity}
                </Text>
                <Text style={{ ...ProductItemStyle.itemStyle }}>
                    {product.price.toLocaleString('en')}
                </Text>
            </View>
        );
    }
}


export default ProductInfoRecord;
