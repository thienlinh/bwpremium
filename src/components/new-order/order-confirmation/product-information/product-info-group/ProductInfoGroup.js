import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';

import { SectionTitle } from '../../../../commons';
import ProductInfoRecord from './product-info-record/ProductInfoRecord';

const { height } = Dimensions.get('window');

class ProductInfoGroup extends Component {
    renderItems() {
        return this.props.group.list.map(i => (
            <ProductInfoRecord
                key={`${i.filmSize} ${i.filmType}`}
                product={i}
            />
        ));
    }
    render() {
        const { group } = this.props;
        return (
            <View
                style={styles.groupStyle}
            >
                <SectionTitle
                    fontSize={16}
                >
                    {group.groupName}
                </SectionTitle>

                {this.renderItems()}
            </View>
        );
    }
}

const styles = {
    groupStyle: {
        marginBottom: height * 0.01,
        flexDirection: 'column'
    }
};

export default ProductInfoGroup;
