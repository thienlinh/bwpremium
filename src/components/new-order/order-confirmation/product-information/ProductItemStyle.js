import { Dimensions } from 'react-native';

const { height } = Dimensions.get('window');

const ProductItemStyle = {
    recordStyle: {
        flexDirection: 'row',
        marginBottom: height * 0.005
    },
    filmNameItemStyle: {
        flex: 4
    },
    itemStyle: {
        flex: 1,
        fontWeight: '500',
        textAlign: 'right'
    }
};

export default ProductItemStyle;
