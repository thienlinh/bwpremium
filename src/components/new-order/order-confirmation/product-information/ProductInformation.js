import React, { Component } from 'react';
import { View } from 'react-native';

import ProductInfoWrapper from './product-info-wrapper/ProductInfoWrapper';
import ProductInfoGroup from './product-info-group/ProductInfoGroup';
import * as commonHelpers from '../../../../helpers/CommonHelpers';

class ProductInformation extends Component {
    renderGroups() {
        const filteredGroup = commonHelpers.filteredProductGroup(this.props.order.products);

        return filteredGroup.map(g => (
            <ProductInfoGroup
                key={g.groupName}
                group={g}
            />
        ));
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ProductInfoWrapper order={this.props.order}>
                    {this.renderGroups()}
                </ProductInfoWrapper>

            </View>
        );
    }
}

const styles = {
    containerStyle: {
        marginLeft: 20,
        flexDirection: 'column'
    }
};

export default ProductInformation;
