import React, { Component } from 'react';
import { Divider } from '@shoutem/ui';
import { ScrollView, TouchableWithoutFeedback, View, Text } from 'react-native';
import sum from 'lodash.sum';

import { SectionTitle } from '../../../../commons';
import ProductItemStyle from '../ProductItemStyle';
import ProductInfoRecord from '../product-info-group/product-info-record/ProductInfoRecord';
import * as titleConstants from '../../../../../constants/TitleConstants';

class ProductInfoWrapper extends Component {

    renderOption() {
        const { order } = this.props;
        const quantity = sum(order.products.map(p => p.quantity));
        const optionList = [];
        if (order.border) {
            optionList.push({ title: titleConstants.BORDER, quantity, price: 0 });
        }

        if (order.quickProcessing) {
            optionList.push({ title: titleConstants.QUICK_PROCESSING, quantity, price: 0 });
        }

        if (order.push !== 0) {
            optionList.push({ title: titleConstants.PUSH, quantity, price: 0 });
        }

        if (order.freeCount !== 0) {
            optionList.push({
                title: titleConstants.FREE_COUNT,
                quantity: order.freeCount,
                price: -order.freePrice
            });
        }

        return (optionList.length > 0 &&
            <View>
                <Divider
                    styleName="line"
                    style={styles.dividerStyle}
                />
                {optionList.map(i => <ProductInfoRecord
                    product={i}
                    key={i.title}
                />)}
            </View>
        );
    }
    render() {
        return (
            <View>
                <View style={ProductItemStyle.recordStyle}>
                    <View style={ProductItemStyle.filmNameItemStyle} />
                    <Text style={{ ...ProductItemStyle.itemStyle, ...styles.titleStyle }}>
                        {titleConstants.QUANTITY_SHORT}
                    </Text>
                    <Text style={{ ...ProductItemStyle.itemStyle, ...styles.titleStyle }}>
                        {titleConstants.CURRENCY}
                    </Text>
                </View>

                <ScrollView
                    style={styles.childrenContainerStyle}
                    showsVerticalScrollIndicator
                >
                    <TouchableWithoutFeedback>
                        <View>
                            {this.props.children}
                            {this.renderOption()}
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>

                <Divider
                    styleName="line"
                    style={styles.dividerStyle}
                />
                <View style={{ ...ProductItemStyle.recordStyle, marginTop: 5 }}>
                    <SectionTitle
                        fontSize={16}
                        style={ProductItemStyle.filmNameItemStyle}
                    >
                        {titleConstants.TOTAL}
                    </SectionTitle>
                    <View style={{ flex: 1 }} />
                    <Text style={{ ...ProductItemStyle.itemStyle }}>
                        {this.props.order.totalPrice.toLocaleString('en')}
                    </Text>
                </View>
            </View>


        );
    }
}

const styles = {
    titleStyle: {
        fontWeight: '500',
        fontSize: 16,
        color: 'black',
    },
    childrenContainerStyle: {
        maxHeight: 220,

    },
    dividerStyle: {
        height: 1.5,
        backgroundColor: 'black',
        marginTop: 3,
        marginBottom: 5
    }
};

export default ProductInfoWrapper;
