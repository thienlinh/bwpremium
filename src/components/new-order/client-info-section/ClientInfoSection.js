import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import { InputField } from '../../commons';
import * as constants from '../../../constants/TitleConstants';
import {
    emailChanged,
    phoneNumberChanged,
    nameChanged,
} from '../../../actions';

class ClientInfoSection extends Component {

    onChangePhoneNumber(text) {
        this.props.phoneNumberChanged(text);
    }

    onChangeName(text) {
        this.props.nameChanged(text);
    }

    onChangeEmail(text) {
        this.props.emailChanged(text);
    }

    render() {
        const {
            email,
            name,
            phoneNumber
            } = this.props;

        return (
            <View style={{ ...this.props.style }}>
                <InputField
                    style={styles.inputStyle}
                    label={constants.PHONE_NUMBER}
                    withSpac
                    keyboardType='numeric'
                    onChangeText={this.onChangePhoneNumber.bind(this)}
                    value={phoneNumber}
                />
                <InputField
                    style={styles.inputStyle}
                    label={constants.FULL_NAME}
                    autoCapitalize='words'
                    onChangeText={this.onChangeName.bind(this)}
                    value={name}
                />
                <InputField
                    style={styles.inputStyle}
                    label={constants.EMAIL}
                    keyboardType='email-address'
                    onChangeText={this.onChangeEmail.bind(this)}
                    value={email}
                />
            </View>
        );
    }
}

const styles = {
    inputStyle: {
        marginBottom: 10
    }
};

const mapStateToProps = ({ orderCreation }) => {
    const { email, phoneNumber, name } = orderCreation.order;

    return { email, phoneNumber, name };
};

export default connect(mapStateToProps, {
    emailChanged,
    phoneNumberChanged,
    nameChanged,
})(ClientInfoSection);
