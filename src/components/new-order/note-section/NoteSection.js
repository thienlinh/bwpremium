import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';

import { InputField } from '../../commons';
import * as tileConstants from '../../../constants/TitleConstants';
import {
    noteChanged
} from '../../../actions';

class NoteSection extends Component {

    onChangeNote(text) {
        this.props.noteChanged(text);
    }

    render() {
        const { note } = this.props;
        return (
            <View style={this.props.style}>
                <InputField
                    style={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                    multiline
                    numberOfLines={4}
                    label={`${tileConstants.BULLET_CHAR} ${tileConstants.NOTE}`}
                    textStyle={styles.textStyle}
                    onChangeText={this.onChangeNote.bind(this)}
                    value={note}
                />
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        height: 150,
        flexDirection: 'column'
    },
    inputStyle: {
        flex: 1,
        padding: 10
    },
    textStyle: {
        fontWeight: '500',
        fontSize: 18,
        color: 'black',
        flex: 0,
        marginBottom: 10
    }
};

const mapStateToProps = ({ orderCreation }) => {
    const { note } = orderCreation.order;

    return { note };
};

export default connect(mapStateToProps, {
    noteChanged
})(NoteSection);
