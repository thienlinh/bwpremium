import React, { Component } from 'react';
import { View, Text } from '@shoutem/ui';

import * as productSlectionStyles from './ProductSelectionStyle';
import * as titleConstants from '../../../constants/TitleConstants';

class ProductHeader extends Component {

    render() {
        const { isBw } = this.props;
        return (

            <View
                styleName="horizontal"
            >

                <View style={{ ...styles.itemStyle, flex: 3 }} />
                <Text
                    style={{
                        ...styles.headerItemStyle,
                        ...styles.itemStyle,
                        ...styles.textHeaderStyle,
                    }}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >
                    {titleConstants.DEV}
                </Text>
                <Text
                    style={{
                        ...styles.headerItemStyle,
                        ...styles.itemStyle,
                        ...styles.textHeaderStyle,
                    }}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >
                    {titleConstants.SCAN}
                </Text>
                {isBw && <Text
                    style={{
                        ...styles.headerItemStyle,
                        ...styles.itemStyle,
                        ...styles.textHeaderStyle,
                    }}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >
                    {titleConstants.PROOF_PRINT}
                </Text>}
                <Text
                    style={{
                        ...styles.headerItemStyle,
                        ...styles.itemStyle,
                        ...styles.textHeaderStyle,
                    }}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >
                    {titleConstants.QUANTITY}
                </Text>
            </View>
        );
    }
}

const styles = {
    ...productSlectionStyles.productSlectionStyles,
    headerItemStyle: {
        backgroundColor: 'black',
        color: 'white',
        height: 25,
        paddingTop: 3
    },

    textHeaderStyle: {
        textAlign: 'center',

    },
    selectorLabelHeaderStyle: {
        marginRight: 0,
        width: 60,

    },
    test: {
        fontWeight: '500',
        fontSize: 18,
        color: 'black',
        marginBottom: 10
    }
};

export default ProductHeader;
