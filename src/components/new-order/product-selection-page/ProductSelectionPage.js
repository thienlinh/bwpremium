import React, { Component } from 'react';
import { View } from '@shoutem/ui';
import { LayoutAnimation } from 'react-native';

import FilmRow from './FilmRow';
import ProductHeader from './ProductHeader';
import * as valueConstants from '../../../constants/ValueConstants';

class ProductSelectionPage extends Component {

    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    render() {
        const { filmType, productPageStyle, isBw, products } = this.props;

        return (
            <View>
                <ProductHeader
                    isBw={isBw}
                />
                <View style={{ ...productPageStyle }}>
                    <FilmRow
                        filmType={filmType}
                        filmSize={valueConstants.FILM_SIZE_135MM}
                        isBw={isBw}
                        product={products.find(p => p.filmType === filmType &&
                            p.filmSize === valueConstants.FILM_SIZE_135MM)}
                    />
                    <FilmRow
                        filmType={filmType}
                        filmSize={valueConstants.FILM_SIZE_120MM}
                        isBw={isBw}
                        product={products.find(p => p.filmType === filmType &&
                            p.filmSize === valueConstants.FILM_SIZE_120MM)}
                    />
                    <FilmRow
                        filmType={filmType}
                        filmSize={valueConstants.FILM_SIZE_LF45}
                        isBw={isBw}
                        product={products.find(p => p.filmType === filmType &&
                            p.filmSize === valueConstants.FILM_SIZE_LF45)}
                    />
                    <FilmRow
                        filmType={filmType}
                        filmSize={valueConstants.FILM_SIZE_LF810}
                        isBw={isBw}
                        product={products.find(p => p.filmType === filmType &&
                            p.filmSize === valueConstants.FILM_SIZE_LF810)}
                    />
                </View>
            </View>
        );
    }
}


export default ProductSelectionPage;
