export const productSlectionStyles = {
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemStyle: {
        flex: 1,
        alignItems: 'center',
        marginLeft: 1,

    },
    selectorStyle: {
        width: 50,
        backgroundColor: 'white',
        height: 35
    },
    textStyle: {
    },
    firstCheckboxStyle: {
        width: 20,
        marginRight: 10,
    }
};
