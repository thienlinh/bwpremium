import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { CheckBox } from 'react-native-elements';
import ModalSelector from 'react-native-modal-selector';

import * as valueConstants from '../../../constants/ValueConstants';
import * as productSlectionStyles from './ProductSelectionStyle';
import {
    filmRowChecked,
    devChecked,
    scanChecked,
    proofprintChecked,
    quantityChanged

} from '../../../actions';

class FilmRow extends Component {

    onFilmRowChecked(isChecked, filmType, filmSize) {
        this.props.filmRowChecked(!isChecked, filmType, filmSize);
    }

    onProofprintChecked(isChecked, filmType, filmSize) {
        this.props.proofprintChecked(!isChecked, filmType, filmSize);
    }

    onDevChecked(isChecked, filmType, filmSize) {
        this.props.devChecked(!isChecked, filmType, filmSize);
    }

    onScanChecked(isChecked, filmType, filmSize) {
        this.props.scanChecked(!isChecked, filmType, filmSize);
    }

    onQuantityChanged(value, filmType, filmSize) {
        this.props.quantityChanged(value, filmType, filmSize);
    }

    render() {
        const { filmType, filmSize, isBw, product } = this.props;

        return (

            <View style={styles.containerStyle} >
                <CheckBox
                    style={{ ...styles.firstCheckboxStyle, flex: 3 }}
                    checked={product !== undefined && product.rowChecked}
                    title={`${filmSize} ${filmType}`}
                    onPress={() =>
                        this.onFilmRowChecked(product !== undefined && product.rowChecked,
                            filmType,
                            filmSize)}
                    checkedColor='black'
                    uncheckedColor='black'
                />
                <CheckBox
                    style={styles.itemStyle}
                    checked={product !== undefined && product.dev}
                    center
                    onPress={() =>
                        this.onDevChecked(product !== undefined && product.dev,
                            filmType,
                            filmSize)}
                    checkedColor='black'
                    uncheckedColor='black'
                />
                <CheckBox
                    style={styles.itemStyle}
                    checked={product !== undefined && product.scan}
                    center
                    onPress={() =>
                        this.onScanChecked(product !== undefined && product.scan,
                            filmType,
                            filmSize)}
                    checkedColor='black'
                    uncheckedColor='black'
                />
                {isBw && <CheckBox
                    style={styles.itemStyle}
                    checked={product !== undefined && product.proofprint}
                    center
                    onPress={() =>
                        this.onProofprintChecked(product !== undefined && product.proofprint,
                            filmType,
                            filmSize)}
                    checkedColor='black'
                    uncheckedColor='black'
                />}
                <ModalSelector
                    style={styles.itemStyle}
                    selectStyle={styles.selectorStyle}
                    data={valueConstants.quantityValues()}
                    initValue={`${product !== undefined &&
                        product.quantity > 0 ? product.quantity : ''}`}
                    onChange={(option) => this.onQuantityChanged(option.key, filmType, filmSize)}
                />
            </View>
        );
    }
}


const styles = productSlectionStyles.productSlectionStyles;

const mapStateToProps = ({ orderCreation }) => {
    const { products } = orderCreation.order;
    return { products };
};

export default connect(mapStateToProps, {
    filmRowChecked,
    devChecked,
    scanChecked,
    proofprintChecked,
    quantityChanged,
})(FilmRow);
