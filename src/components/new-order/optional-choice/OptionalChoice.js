import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CheckBox } from 'react-native-elements';
import { View, Text } from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import { MediaQueryStyleSheet } from 'react-native-responsive';

import * as tileConstants from '../../../constants/TitleConstants';
import * as valueConstants from '../../../constants/ValueConstants';
import {
    packageChanged,
    quickProcessingChanged,
    borderChanged,
    pushChanged
} from '../../../actions';
import { IPHONE_6_PLUS } from '../../../style/breakpoints';

class OptionalChoice extends Component {

    onPackageChecked(type) {
        this.props.packageChanged(type === valueConstants.ORDER_TYPE_PACKAGE ?
            valueConstants.ORDER_TYPE_DIRECT :
            valueConstants.ORDER_TYPE_PACKAGE);
    }

    onQuickProcessingChecked(isChecked) {
        this.props.quickProcessingChanged(!isChecked);
    }

    onBorderChecked(isChecked) {
        this.props.borderChanged(!isChecked);
    }

    onPushChanged(option) {
        this.props.pushChanged(option);
    }

    render() {
        const { type, border, quickProcessing, push } = this.props;
        return (
            <View
                style={styles.containerStyle}
            >
                <View style={styles.columnStyle}>
                    <CheckBox
                        title={tileConstants.PACKAGE}
                        checkedColor='black'
                        uncheckedColor='black'
                        containerStyle={styles.checkboxStyle}
                        textStyle={{ color: 'black' }}
                        checked={type === valueConstants.ORDER_TYPE_PACKAGE}
                        onPress={() => this.onPackageChecked(type)}
                    />
                    <CheckBox
                        title={tileConstants.QUICK_PROCESSING}
                        checkedColor='black'
                        uncheckedColor='black'
                        containerStyle={styles.checkboxStyle}
                        textStyle={{ color: 'black' }}
                        checked={quickProcessing}
                        onPress={() => this.onQuickProcessingChecked(quickProcessing)}
                    />
                </View>
                <View style={styles.columnStyle}>
                    <CheckBox
                        title={tileConstants.BORDER}
                        checkedColor='black'
                        uncheckedColor='black'
                        containerStyle={styles.checkboxStyle}
                        textStyle={{ color: 'black' }}
                        checked={border}
                        onPress={() => this.onBorderChecked(border)}
                    />
                    <View style={styles.selectorContainerStyle}>
                        <ModalSelector
                            style={{ marginRight: 5 }}
                            selectStyle={{
                                width: 40,
                                backgroundColor: 'white',
                                height: 35,
                                marginRight: 0
                            }}
                            data={valueConstants.pushValues()}
                            initValue={`${push !== 0 ? push : ''}`}
                            onChange={(option) => this.onPushChanged(option.key)}
                        />
                        <Text style={styles.selectorTextStyle}>
                            {tileConstants.PUSH}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = MediaQueryStyleSheet.create({
    containerStyle: {
        flexDirection: 'row'
    },
    columnStyle: {
        flexDirection: 'column',
    },
    checkboxStyle: {
        backgroundColor: 'transparent',
        borderWidth: 0,
        marginBottom: 5,
        padding: 0
    },
    selectorContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    selectorTextStyle: {
        fontWeight: '500',
        color: 'black',
    }
},
    {
        [IPHONE_6_PLUS]: {
            columnStyle: {
                flexDirection: 'column',
                maxWidth: 100
            },
        }
    }
);

const mapStateToProps = ({ orderCreation }) => {
    const { type, border, quickProcessing, push } = orderCreation.order;

    return { type, border, quickProcessing, push };
};

export default connect(mapStateToProps, {
    packageChanged,
    quickProcessingChanged,
    borderChanged,
    pushChanged
})(OptionalChoice);
