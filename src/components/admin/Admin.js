import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import Router from './Router';


class Admin extends Component {

    static navigationOptions = {
        tabBarLabel: 'Login',
        tabBarIcon: () => (<Icon size={24} color="white" name="sign-in" />)
    };

    render() {
        return (
            <Router />
        );
    }
}

export default Admin;

