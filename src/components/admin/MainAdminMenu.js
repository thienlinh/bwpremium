import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Screen, View, Text } from '@shoutem/ui';
import { List, ListItem } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

import * as titleConstants from '../../constants/TitleConstants';
import { AppButton } from '../commons';
import {
    logout
} from '../../actions';

class MainAdminMenu extends Component {

    render() {
        const { user } = this.props;
        const list = [
            {
                title: titleConstants.PENDING,
                icon: 'list',
                press: () => Actions.pending()
            },
            {
                title: titleConstants.REPORT,
                icon: 'flight-takeoff',
                press: () => Actions.report()
            },

        ];
        return (
            <Screen>
                <View
                    style={{ marginTop: 10 }}
                    styleName="horizontal v-center h-end"
                >
                    <Text>
                        {titleConstants.HELLO}
                    </Text>
                    <Text style={styles.userNameStyle}>
                        {user.displayName || user.email}
                    </Text>
                    <AppButton
                        text={titleConstants.LOGOUT}
                        onPress={() => this.props.logout()}
                    />
                </View>
                <List>
                    {
                        list.map((item, i) => (
                            <ListItem
                                key={i}
                                title={item.title}
                                leftIcon={{ name: item.icon }}
                                onPress={item.press}
                            />
                        ))
                    }
                </List>
            </Screen>
        );
    }
}

const styles = {
    userNameStyle: {
        fontWeight: 'bold',
        marginLeft: 10,
        marginRight: 30,
    }
};

const mapStateToProps = ({ auth }) => {
    const { user } = auth;
    return { user };
};

export default connect(mapStateToProps, {
    logout
})(MainAdminMenu);

