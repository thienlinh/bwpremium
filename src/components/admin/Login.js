import React, { Component } from 'react';
import { Image, View, Text, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import { InputField, AppButton, Spinner } from '../commons';
import * as titleConstants from '../../constants/TitleConstants';
import {
    authEmailChanged,
    passwordChanged,
    loginUser
} from '../../actions';

const { width } = Dimensions.get('window');

class Login extends Component {
    renderSpinner() {
        if (this.props.loading) {
            return (<Spinner />);
        }
    }
    render() {
        const { email, password, error } = this.props;
        return (
            <View
                styleName="vertical h-center"
                style={styles.containerStyle}
            >
                <Image
                    style={{ width: width / 2, height: 100 }}
                    source={{ uri: ('../../images/bw-splash.png') }}
                />

                <View
                    style={styles.loginContainer}
                    styleName="vertical h-center"
                >

                    <InputField
                        style={styles.loginItemStyle}
                        label={titleConstants.EMAIL}
                        placeholder={titleConstants.EMAIL_PLACEHOLDER}
                        onChangeText={(text) => this.props.authEmailChanged(text)}
                        value={email}
                    />

                    <InputField
                        secureTextEntry
                        style={styles.loginItemStyle}
                        label={titleConstants.PASSWORD}
                        placeholder={titleConstants.PW_PLACEHOLDER}
                        onChangeText={(text) => this.props.passwordChanged(text)}
                        value={password}
                    />

                    <AppButton
                        text={titleConstants.LOGIN}
                        onPress={() => this.props.loginUser({ email, password })}
                    />

                    <Text style={styles.errorTextStyle}>
                        {error}
                    </Text>


                </View>
                {this.renderSpinner()}
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        backgroundColor: 'white',
        paddingLeft: width / 4,
        paddingRight: width / 4,
        justifyContent: 'space-around',
        flex: 1
    },
    imageStyle: {
        width: width / 2,
        height: 100,
    },
    loginContainer: {

    },
    loginItemStyle: {
        marginBottom: 10
    },
    errorTextStyle: {
        color: 'red'
    }
};

const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;
    return { email, password, error, loading };
};

export default connect(mapStateToProps, {
    authEmailChanged,
    passwordChanged,
    loginUser
})(Login);
