import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Screen, ListView } from '@shoutem/ui';

import OrderListItem from '../../commons/order-list-item/OrderListItem';
import SearchBar from './SearchBar';
import * as colors from '../../../style/colors';
import {
    fetchPendingOrder,
    fetchMessageData
} from '../../../actions';
import { Spinner } from '../../commons';

class PendingList extends Component {
    componentWillMount() {
        this.props.fetchPendingOrder(true);
        this.props.fetchMessageData();
    }

    removeOrder() {

    }

    renderRow(order) {
        return (<OrderListItem
            order={order}
            hasDoneButton
        />
        );
    }

    renderSpinner() {
        const { firstLoad, isFetchingMessages } = this.props;
        if (firstLoad || isFetchingMessages) {
            return (<Spinner />);
        }
    }

    render() {
        const { pendingList, isLoading } = this.props;
        return (

            <Screen>
                <View style={styles.container}>
                    <SearchBar
                        containerStyle={{ marginBottom: 10 }}
                    />
                    <View
                        styleName="vertical h-center"
                        style={{ marginBottom: 40 }}
                    >
                        <ListView
                            data={pendingList}
                            renderRow={this.renderRow.bind(this)}
                            onRefresh={() => this.props.fetchPendingOrder(false)}
                            loading={isLoading}
                        />
                    </View>
                </View>
                {this.renderSpinner()}
            </Screen>

        );
    }
}

const styles = {
    container: {
        backgroundColor: colors.lightGray,
        flex: 1
    },
};

const mapStateToProps = ({ pending }) => {
    const { pendingList, criteria, isLoading, firstLoad, isFetchingMessages } = pending;
    return { pendingList, criteria, isLoading, firstLoad, isFetchingMessages };
};

export default connect(mapStateToProps, {
    fetchPendingOrder,
    fetchMessageData
})(PendingList);
