import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from '@shoutem/ui';

import * as titleConstants from '../../../constants/TitleConstants';
import { InputField } from '../../commons';
import { pendingSearchCriteriaChanged } from '../../../actions';


class SearchBar extends Component {

    render() {
        const { pendingSearchCriteria, containerStyle } = this.props;

        return (
            <View className="vertical h-center">
                <View style={{ ...containerStyle, flexDirection: 'row' }}>

                    <InputField
                        style={styles.searchBoxStyle}
                        placeholder={titleConstants.SEARCH_PLACEHOLDER}
                        onChangeText={(text) => { this.props.pendingSearchCriteriaChanged(text); }}
                        value={pendingSearchCriteria}
                        autoCapitalize='none'
                    />

                </View>
            </View>
        );
    }
}

const styles = {
    container: {

    },
    searchBoxStyle: {
        flex: 5,
        backgroundColor: 'white'
    },
    inputStyle: {
        paddingLeft: 20,
        paddingRight: 20
    }
};

const mapStateToProps = ({ pending }) => {
    const { criteria } = pending;
    return {
        criteria
    };
};

export default connect(mapStateToProps, { pendingSearchCriteriaChanged })(SearchBar);
