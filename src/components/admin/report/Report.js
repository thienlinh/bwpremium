import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ScrollView, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Screen } from '@shoutem/ui';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';


import * as colors from '../../../style/colors';
import ReportGrid from './ReportGrid';
import ReportInfo from './ReportInfo';
import { Spinner } from '../../commons';
import {
    fetchReports
} from '../../../actions';
import * as valueConstants from '../../../constants/ValueConstants';

const { height } = Dimensions.get('window');

class Report extends Component {

    componentWillMount() {
        this.props.fetchReports();
    }

    renderSpinner() {
        const { isLoading } = this.props;
        if (isLoading) {
            return (<Spinner />);
        }
    }

    renderPages(reportList) {
        return (reportList.map(report => (
            <View key={report.createdAt}>
                <View style={styles.page}>

                    <ScrollView
                        showsVerticalScrollIndicator
                    >
                        <TouchableWithoutFeedback>
                            <View style={styles.pageContent}>
                                <ReportInfo
                                    data={report}
                                />
                                <ReportGrid
                                    data={report.detail.find(i =>
                                        i.type === valueConstants.ORDER_TYPE_DIRECT)}
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>


                </View>
            </View>
        )));
    }

    renderDotIndicator(pageCount) {
        return (<PagerDotIndicator
            pageCount={pageCount}
            selectedDotStyle={{ backgroundColor: 'black' }}
        />);
    }

    render() {
        const { reportList } = this.props;
        return (
            <Screen style={styles.container} >
                <View style={{ flex: 1 }}>
                    <IndicatorViewPager
                        style={{ flex: 1 }}
                        indicator={this.renderDotIndicator(reportList.length)}
                        initialPage={reportList.length - 1}
                    >
                        {/* <View style={{ backgroundColor: 'cadetblue' }}>
                            <Text>page one</Text>
                        </View>
                        <View style={{ backgroundColor: 'cornflowerblue' }}>
                            <Text>page two</Text>
                        </View>
                        <View style={{ backgroundColor: '#1AA094' }}>
                            <Text>page three</Text>
                        </View> */}
                        {this.renderPages(reportList)}
                    </IndicatorViewPager>

                </View>
            </Screen>

        );
    }


}

const styles = {
    container: {
        // backgroundColor: colors.lightGray,
        flex: 1
    },
    page: {
        flex: 1,
        backgroundColor: colors.lightGray,
        padding: 10,
    },
    pageContent: {
        backgroundColor: 'white',
        padding: 10,
        flex: 1,
        minHeight: height * 0.8
    }
};

const mapStateToProps = ({ dailyReport }) => {
    const { isLoading, reportList } = dailyReport;
    return { reportList, isLoading };
};

export default connect(mapStateToProps, {
    fetchReports
})(Report);

