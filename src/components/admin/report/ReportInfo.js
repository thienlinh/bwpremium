import React, { Component } from 'react';
import { View } from '@shoutem/ui';
import moment from 'moment';

import * as titleConstants from '../../../constants/TitleConstants';
import * as commonFormats from '../../../constants/CommonFormats';
import { SectionTitle } from '../../commons';

class ReportInfo extends Component {
    render() {
        const { data } = this.props;

        return (
            <View>
                <SectionTitle>
                    {moment.unix(data.createdAt / 1000).format(commonFormats.dateFormat)}
                </SectionTitle>
            </View>
        );
    }
}

const styles = {

};

export default ReportInfo;
