import React, { Component } from 'react';
import { View } from 'react-native';
import { Table, Row, TableWrapper, Cell } from 'react-native-table-component';
import orderBy from 'lodash.orderby';
import { MediaQueryStyleSheet } from 'react-native-responsive';

import * as titleConstants from '../../../constants/TitleConstants';
import { IPHONE_6_PLUS } from '../../../style/breakpoints';

class ReportGrid extends Component {


    buildTable(reportList) {
        // TODO do it later

        // const groups = [];
        // valueConstants.filmTypeList.forEach(filmType => {
        //     const itemList = reportList.filter(i => i.filmType === filmType);
        //     const grid = (<TableWrapper style={{ flexDirection: 'row' }} key={filmType}>
        //         <Col
        //             data={[filmType]}
        //             style={styles.title}
        //             textStyle={styles.titleText}
        //         />
        //         {valueConstants.filmSizeList.forEach(filmSize => {
        //             itemList.filter(i => i.filmSize === filmSize).map(item => (
        //                 <TableWrapper style={{ flexDirection: 'row' }}>
        //                     <Col
        //                         data={[filmSize]}
        //                         style={styles.title}
        //                         textStyle={styles.titleText}
        //                     />
        //                 </TableWrapper>)
        //             );
        //         })
        //         }
        //     </TableWrapper>);

        //     groups.push(grid);
        // });

        // return groups;

        return reportList.map(report => {
            const service = `${(report.dev && report.scan && report.proofprint) ?
                titleConstants.ALL_THREE_GROUP :
                (report.dev && report.scan) ?
                    titleConstants.DEV_SCAN_GROUP :
                    (report.dev && report.proofprint) ?
                        titleConstants.DEV_PROOFPRINT_GROUP :
                        (report.scan && report.proofprint) ?
                            titleConstants.SCAN_PROOFPRINT_GROUP :
                            report.dev ? titleConstants.DEV_GROUP :
                                report.scan ? titleConstants.SCAN_GROUP :
                                    titleConstants.PROOFPRINT_GROUP}`;

            return (<Row
                key={`${report.filmType} ${report.filmSize} ${service}`}
                style={styles.row}
                flexArr={[1, 1, 1, 1]}
                data={[
                    report.filmType,
                    report.filmSize,
                    service,
                    report.quantity,
                    report.price.toLocaleString('en')]}
                textStyle={styles.rowText}
            />
            );
        });
    }

    render() {
        const tableHead = [
            titleConstants.FILM_TYPE,
            titleConstants.FILM_SIZE,
            titleConstants.FILM_SERVICE,
            titleConstants.QUANTITY,
            titleConstants.PRICE
        ];

        const { data } = this.props;

        return (
            <View>
                <Table>
                    <Row
                        data={tableHead}
                        style={styles.header}
                        textStyle={styles.headerText}
                    />

                    {this.buildTable(orderBy(data.list, ['filmType', 'filmSize']))}

                    <TableWrapper style={{ flexDirection: 'row' }}>
                        <Cell
                            data={titleConstants.TOTAL}
                            style={styles.wideCell}
                            textStyle={styles.highlightTitle}
                        />
                        <Row
                            style={styles.normalCell}
                            data={[data.total ? data.total.toLocaleString('en') : '']}
                            textStyle={styles.rowText}
                        />
                    </TableWrapper>

                </Table>
            </View>
        );
    }

}

const styles = MediaQueryStyleSheet.create(
    {
        header: {
            height: 30,
            backgroundColor: 'black'
        },
        headerText: {
            textAlign: 'center',
            color: 'white',
            fontWeight: 'bold'
        },
        row: {
            height: 30
        },
        rowText: {
            textAlign: 'center',
        },
        title: {
            flex: 1,
        },
        titleText: {
            textAlign: 'center',
        },
        wideCell: {
            height: 30,
            flex: 4
        },
        highlightTitle: {
            textAlign: 'center',
            fontWeight: 'bold'
        },
        normalCell: {
            height: 30,
            flex: 1
        }
    },
    {
        [IPHONE_6_PLUS]: {
            header: {
                height: 70,
            },
            row: {
                height: 70
            },
            wideCell: {
                height: 70,
            },
            normalCell: {
                height: 70,
            }
        }
    });

export default ReportGrid;
