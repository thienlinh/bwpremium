import React from 'react';
import { Scene, Router } from 'react-native-router-flux';

import Login from './Login';
import MainAdminMenu from './MainAdminMenu';
import * as titleConstants from '../../constants/TitleConstants';
import PendingList from './pending/PendingList';
import Report from './report/Report';

const RouterComponent = () => (
    <Router>
        <Scene
            navigationBarStyle={{ backgroundColor: 'black' }}
            titleStyle={{ color: 'white' }}
        >
            <Scene
                key="login"
                component={Login}
                title={titleConstants.LOGIN}
                
            />
            <Scene
                key="menu"
                component={MainAdminMenu}
                title={titleConstants.HOME}
                initial
            />
            <Scene
                key="pending"
                component={PendingList}
                title={titleConstants.PENDING_UPPER}

            />
            <Scene
                key="report"
                component={Report}
                title={titleConstants.REPORT_UPPER}

            />
        </Scene>
    </Router>
);

export default RouterComponent;
