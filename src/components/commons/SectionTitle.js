import React, { Component } from 'react';
import { Text } from '@shoutem/ui';

class SectionTitle extends Component {
    render() {
        return (
            <Text
                style={{
                    ...styles.tileStyle,
                    ...this.props.style,
                    fontSize: this.props.fontSize || 18,
                    fontWeight: this.props.fontWeight || '500',
                }}
            >
                {this.props.children}
            </Text>
        );
    }
}

const styles = {
    tileStyle: {
        color: 'black',
        marginBottom: 10
    }
};

export { SectionTitle };
