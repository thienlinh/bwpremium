import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';

class InputField extends Component {

    renderLabel(label) {
        if (label) {
            return (
                <Text
                    style={{ ...styles.textStyle, ...this.props.textStyle }}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >
                    {label}
                </Text>);
        }
    }

    renderSpace(withSpace) {
        if (withSpace) {
            return (
                <View style={{ ...this.props.spaceStyle, flex: 1 }} />
            );
        }
    }

    render() {
        const {
            label,
            value,
            keyboardType,
            multiline,
            numberOfLines,
            withSpace,
            onChangeText,
            autoCapitalize,
            placeholder,
            secureTextEntry
        } = this.props;

        return (
            <View style={{ ...styles.containerStyle, ...this.props.style }}>

                {this.renderLabel(label)}

                <View style={styles.containerInputStyle}>
                    <TextInput
                        multiline={multiline}
                        keyboardType={keyboardType || 'default'}
                        style={{ ...styles.inputStyle, ...this.props.inputStyle }}
                        numberOfLines={numberOfLines || 1}
                        onChangeText={onChangeText}
                        value={value}
                        autoCorrect={false}
                        autoCapitalize={autoCapitalize || 'none'}
                        placeholder={placeholder}
                        secureTextEntry={secureTextEntry}
                    />
                </View>

                {this.renderSpace(withSpace)}
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        flexDirection: 'row',
        height: 30,
    },
    textStyle: {
        flex: 1,
        marginTop: 5,
    },
    inputStyle: {
        flex: 1,
        borderColor: 'gray',
        borderWidth: 1,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: 'white'
    },
    containerInputStyle: {
        flex: 2,
        height: 35,
    }
};

export { InputField };
