import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';

class Spinner extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.loadingContainer}>

                    <ActivityIndicator
                        animating
                        size='large'
                        color='white'
                    />

                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },

    loadingContainer: {
        zIndex: 9999,
        padding: 30,
        backgroundColor: 'black',
        borderRadius: 8,
        opacity: 0.8,
        justifyContent: 'center',
        alignItems: 'center'
    },
};

export { Spinner };
