import React, { Component } from 'react';
import { NavigationBar, Title, View } from '@shoutem/ui';

class Header extends Component {
    render() {
        const { title } = this.props;

        return (
            <View style={styles.containerStyle}>
                <NavigationBar
                    styleName="clear"
                    centerComponent={
                        <Title
                            style={{ minWidth: 200, textAlign: 'center' }}
                        >
                            {title}
                        </Title>
                    }
                />
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        backgroundColor: 'black',
        height: 64
    }
};

export { Header };
