import React, { Component } from 'react';
import { Button, Text } from '@shoutem/ui';
import Icon from 'react-native-vector-icons/FontAwesome';

class AppButton extends Component {
    render() {
        const { text, onPress, style, textStyle, icon, type = 'primary' } = this.props;
        const buttonStyle = type === 'secondary'
            ? { ...styles.secondaryButtonStyle, ...style }
            : { ...styles.primaryButtonStyle, ...style };

        const buttonTextStyle = type === 'secondary'
            ? { ...styles.secondaryTextStyle, ...textStyle }
            : { ...styles.primaryTextStyle, ...textStyle };
        return (
            <Button
                style={buttonStyle}
                onPress={onPress}
            >
                {
                    (!icon
                        && <Text style={buttonTextStyle}>
                            {text}
                        </Text>)
                    || <Icon size={24} color="white" name={icon} />
                }
            </Button>
        );
    }
}

const styles = {
    primaryButtonStyle: {
        backgroundColor: 'black',
    },
    secondaryButtonStyle: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black'
    },
    primaryTextStyle: {
        color: 'white',
    },
    secondaryTextStyle: {
        color: 'black',
    }
};

export { AppButton };
