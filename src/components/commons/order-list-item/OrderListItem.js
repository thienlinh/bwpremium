import React, { Component } from 'react';
import { MediaQueryStyleSheet } from 'react-native-responsive';
import moment from 'moment';
import { connect } from 'react-redux';
import {
    Dimensions,
    TouchableOpacity,
    Alert,
    TouchableHighlight,
    View,
    Text
} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import { phonecall } from 'react-native-communications';

import * as tileConstants from '../../../constants/TitleConstants';
import * as commonHelpers from '../../../helpers/CommonHelpers';
import ProductBasicInfoGroup from './ProductBasicInfoGroup';
import { AppButton } from '../../commons';
import {
    donePendingOrder,
    deletePendingOrder
} from '../../../actions';
import * as messageConstants from '../../../constants/MessageConstants';
import * as valueConstants from '../../../constants/ValueConstants';
import * as commonFormats from '../../../constants/CommonFormats';
import { LARGE_WIDTH } from '../../../style/breakpoints';

const { width } = Dimensions.get('window');

class OrderListItem extends Component {
    onDeleteOrder() {
        const { order } = this.props;
        if (!this.props.hasDoneButton) {
            return;
        }
        Alert.alert(messageConstants.deleteOrderTitle, messageConstants.deleteOrderMessage, [
            { text: 'OK', onPress: () => this.props.deletePendingOrder(order) },
            { text: 'Cancel' }
        ]);
    }

    onCall(phoneNumber) {
        phonecall(phoneNumber, true);
    }

    render() {
        const { order, hasDoneButton } = this.props;
        const groupList = commonHelpers.filteredProductGroup(order.products);
        return (

            <TouchableOpacity
                onLongPress={() => this.onDeleteOrder()}
            >
                <View
                    style={styles.container}
                >
                    <View
                        style={styles.firstSection}
                    >
                        <Text>
                            {moment.unix(order.createdAt / 1000)
                                .format(commonFormats.dateTimeFormat)}
                        </Text>
                        <Text
                            style={styles.highlightLargeTextStyle}
                        >
                            {order.orderNumber}
                        </Text>
                    </View>
                    <Text
                        style={styles.textWithBottomSpace}
                    >
                        {order.name}
                    </Text>
                    <Text
                        style={styles.textWithBottomSpace}
                    >
                        {order.email}
                    </Text>
                    <TouchableHighlight
                        onPress={() => this.onCall(order.phoneNumber)}
                    >
                        <Text
                            style={styles.underlineText}
                        >
                            {order.phoneNumber}
                        </Text>
                    </TouchableHighlight>
                    {groupList.map(g => <ProductBasicInfoGroup key={g.groupName} group={g} />)}
                    <Text
                        style={styles.textWithBottomSpace}
                    >
                        {`${tileConstants.NOTE}:`}
                    </Text>
                    <Text
                        style={{ marginLeft: 5 }}
                    >
                        {order.note}
                    </Text>

                    {hasDoneButton && <View
                        style={styles.lastSection}
                    >

                        <ModalSelector
                            data={valueConstants.laguageSelection}
                            onChange={(option) => this.props.donePendingOrder(order, option.key)}
                        >
                            <AppButton
                                style={{ width: 100 }}
                                text={tileConstants.DONE}
                            />
                        </ModalSelector>
                    </View>}
                </View>
            </TouchableOpacity>

        );
    }
}

const styles = MediaQueryStyleSheet.create(
    //Base styles:
    {
        container: {
            backgroundColor: 'white',
            padding: 20,
            marginBottom: 10,
            width: width * 0.6
        },
        highlightTextStyle: {
            fontWeight: '500'
        },
        highlightLargeTextStyle: {
            fontWeight: '800',
            fontSize: 16
        },
        sectionStyle: {
            marginBottom: 10
        },
        textWithBottomSpace: {
            marginBottom: 10,
            fontWeight: '500'
        },
        underlineText: {
            marginBottom: 10,
            textDecorationLine: 'underline'
        },
        firstSection: {
            marginBottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between'
        },
        lastSection: {
            flexDirection: 'row',
            justifyContent: 'center'
        }
    },
    //Media Queries styles:
    {
        [LARGE_WIDTH]: {
            container: {
                width: width * 0.95,

            }
        }
    }
);

export default connect(null, {
    donePendingOrder,
    deletePendingOrder
})(OrderListItem);
