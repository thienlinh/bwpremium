import React, { Component } from 'react';
import { View, Text } from '@shoutem/ui';

class ProductBasicInfoRecord extends Component {
    render() {
        const { product } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.itemStyle}>
                    {`${product.filmSize} ${product.filmType}`}
                </Text>
                <Text style={styles.itemStyle}>
                    {product.quantity}
                </Text>
                {/* <View style={{ flex: 1 }} /> */}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    itemStyle: {
        flex: 1,
        marginLeft: 20
    },
};

export default ProductBasicInfoRecord;
