import React, { Component } from 'react';
import { View, Text } from '@shoutem/ui';

import ProductBasicInfoRecord from './ProductBasicInfoRecord';

class ProductBasicInfoGroup extends Component {
    render() {
        const { group } = this.props;
        return (
            <View style={styles.sectionStyle}>
                <Text
                    style={styles.highlightTextStyle}
                >
                    {group.groupName}
                </Text>

                {group.list.map(i =>
                    <ProductBasicInfoRecord key={`${i.filmSize} ${i.filmType}`} product={i} />)}

            </View>
        );
    }
}

const styles = {

    highlightTextStyle: {
        fontWeight: '500'
    },
    sectionStyle: {
        marginBottom: 10
    }

};

export default ProductBasicInfoGroup;
