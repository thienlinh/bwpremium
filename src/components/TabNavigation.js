import { NavigationComponent } from 'react-native-material-bottom-navigation';
import { TabNavigator } from 'react-navigation';

import NewOrder from './new-order/NewOrder';
import SearchingOrder from './searching_order/SearchingOrder';
import Admin from './admin/Admin';

const TabNavigation = TabNavigator(
    {
        NewOrder: { screen: NewOrder },
        SearchingOrder: { screen: SearchingOrder },
        Admin: { screen: Admin },
    },
    {
        tabBarComponent: NavigationComponent,
        tabBarPosition: 'bottom',
        tabBarOptions: {
            bottomNavigationOptions: {
                labelColor: 'white',
                rippleColor: 'white',
                tabs: {
                    NewOrder: {
                        barBackgroundColor: '#212121'
                    },
                    SearchingOrder: {
                        barBackgroundColor: '#616161'
                    },
                    Admin: {
                        barBackgroundColor: '#424242'
                    }
                }
            }
        }
    });


export default TabNavigation;
