import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity, Keyboard } from 'react-native';
import { View, Text } from '@shoutem/ui';
import Autocomplete from 'react-native-autocomplete-input';

import * as titleConstants from '../../constants/TitleConstants';
import { AppButton } from '../commons';
import { searchCriteriaChanged, searchOrder } from '../../actions';


class SearchBar extends Component {

    render() {
        const { criteria, containerStyle } = this.props;

        return (
            <View className="vertical h-center">
                <View style={{ ...containerStyle, flexDirection: 'row' }}>

                    <Autocomplete
                        containerStyle={styles.searchBoxStyle}
                        inputContainerStyle={styles.inputStyle}
                        placeholder={titleConstants.SEARCH_PLACEHOLDER}
                        onChangeText={(text) => { this.props.searchCriteriaChanged(text); }}
                        value={criteria}
                        autoCapitalize='none'
                        autoComplete="false"
                        renderItem={data => (
                            <TouchableOpacity onPress={() => { }}>
                                <Text>{data}</Text>
                            </TouchableOpacity>
                        )}
                    />
                    <AppButton
                        style={{ flex: 1 }}
                        icon="search"
                        onPress={() => {
                            Keyboard.dismiss();
                            this.props.searchOrder(criteria);
                        }}
                    />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {

    },
    searchBoxStyle: {
        flex: 5,
        backgroundColor: 'white'
    },
    inputStyle: {
        paddingLeft: 20,
        paddingRight: 20
    }
};

const mapStateToProps = ({ orderSearching }) => {
    const { criteria } = orderSearching;
    return {
        criteria
    };
};

export default connect(mapStateToProps, { searchCriteriaChanged, searchOrder })(SearchBar);
