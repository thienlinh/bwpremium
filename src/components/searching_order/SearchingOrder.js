import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Screen, ListView } from '@shoutem/ui';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Header, Spinner } from '../commons';
import OrderListItem from '../commons/order-list-item/OrderListItem';
import * as titleConstants from '../../constants/TitleConstants';
import SearchBar from './SearchBar';
import * as colors from '../../style/colors';

class SearchingOrder extends Component {

    static navigationOptions = {
        tabBarLabel: 'Search',
        tabBarIcon: () => (<Icon size={24} color="white" name="search" />)
    };

    renderSpinner() {
        const { isLoading } = this.props;
        if (isLoading) {
            return (<Spinner />);
        }
    }

    renderRow(order) {
        return (<OrderListItem
            order={order}
        />
        );
    }

    render() {
        const { data } = this.props;
        return (

            <Screen>
                <Header title={titleConstants.SEARCH_ORDER} />
                <View style={styles.container}>
                    <SearchBar
                        containerStyle={{ marginBottom: 10 }}
                    />
                    <View
                        styleName="vertical h-center"
                        style={{ marginBottom: 40 }}
                    >
                        <ListView
                            style={{ flex: 0.6 }}
                            data={data}
                            renderRow={this.renderRow.bind(this)}
                        />
                    </View>
                </View>
                {this.renderSpinner()}
            </Screen>

        );
    }
}

const styles = {
    container: {
        backgroundColor: colors.lightGray,
        flex: 1
    },
};

const mapStateToProps = ({ orderSearching }) => {
    const { isLoading, data } = orderSearching;
    return {
        isLoading,
        data
    };
};

export default connect(mapStateToProps, {})(SearchingOrder);
