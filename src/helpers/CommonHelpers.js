
import * as titleConstants from '../constants/TitleConstants';

export const filteredProductGroup = (productList) => {
    return [
        {
            groupName: titleConstants.ALL_THREE_GROUP,
            list: productList.filter(p =>
                p.dev
                && p.scan
                && p.proofprint)
        },
        {
            groupName: titleConstants.DEV_SCAN_GROUP,
            list: productList.filter(p =>
                p.dev
                && p.scan
                && !p.proofprint)
        },
        {
            groupName: titleConstants.DEV_PROOFPRINT_GROUP,
            list: productList.filter(p =>
                p.dev
                && !p.scan
                && p.proofprint)
        },
        {
            groupName: titleConstants.SCAN_PROOFPRINT_GROUP,
            list: productList.filter(p =>
                !p.dev
                && p.scan
                && p.proofprint)
        },

        {
            groupName: titleConstants.DEV_GROUP,
            list: productList.filter(p =>
                p.dev
                && !p.scan
                && !p.proofprint)
        },

        {
            groupName: titleConstants.SCAN_GROUP,
            list: productList.filter(p =>
                !p.dev
                && p.scan
                && !p.proofprint)
        },

        {
            groupName: titleConstants.PROOFPRINT_GROUP,
            list: productList.filter(p =>
                !p.dev
                && !p.scan
                && p.proofprint)
        }]
        .filter(g => g.list.length > 0);
};

