import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';

export const isNullOrUndefined = (object) => {
    return isNull(object) || isUndefined(object);
};

export const isNullOrEmpty = (object) => {
    return isNull(object) || isUndefined(object) || isEmpty(object);
};

export const isDefinedAndNotNull = (object) => {
    return !isNullOrEmpty(object);
};
